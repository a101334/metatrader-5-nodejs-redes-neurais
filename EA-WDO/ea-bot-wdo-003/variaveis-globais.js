const parametros_metatrader = {
    //data_inicial_treino: '2021-01-01',
    //data_final_treino: '2021-06-01',
    data_inicial_treino: '2017-10-01',
    data_final_treino: '2020-12-31',
    fator_lucro_minimo: 1.8,
    resultado_minimo: 20000,
    custo_por_trader: 4,
    desconto_por_trade: 8,
    penalidade_por_erro_trade: 1,
    custo_servidor_cloud: 3000,
    periodo_candles: 21,
    qtd_neuronio_entrada_adicional: 2,
    min_qtd_bars: 1,
    max_qtd_bars_posicao: 94 
};

const parametros_rede_neural = {
    qtd_n_entradas: (parametros_metatrader.periodo_candles - 1)*3 + parametros_metatrader.qtd_neuronio_entrada_adicional,
    redes_saidas: 13
};

parametros_rede_neural.qtd_n_rede_camada_escondida_1 = parseInt( (parametros_rede_neural.qtd_n_entradas)*0.5);
parametros_rede_neural.qtd_n_rede_camada_escondida_2 = parseInt( (parametros_rede_neural.qtd_n_entradas)*0.5);

module.exports = {
    parametros_metatrader,
    parametros_rede_neural
};
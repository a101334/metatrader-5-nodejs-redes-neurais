var fs = require('fs');

const build_neuronios = async (qtd_n_entradas, qtd_n_rede_camada_escondida_1, qtd_n_rede_camada_escondida_2, redes_saidas) => {
    let neuronios = [];
    let camada_1 = [];
    let camada_2 = [];

    const rede = new Promise(async (resolve, reject) => {

        let camada_entrada = [];
        for(let i = 0; i < qtd_n_entradas; i++) {
            camada_entrada[i] = 0;
        }

        camada_entrada[qtd_n_entradas] = 1;

        neuronios.push(camada_entrada); 
        
        for(let i = 0; i < qtd_n_rede_camada_escondida_1; i++) {
            camada_1[i] = 0; 
        }
        
        camada_1[qtd_n_rede_camada_escondida_1] = 1;

        neuronios.push(camada_1);
    
        for(let i = 0; i <= qtd_n_rede_camada_escondida_2; i++) {
            camada_2[i] = 0; 
        }
        
        camada_2[qtd_n_rede_camada_escondida_2] = 1;

        neuronios.push(camada_2);

        let camada_saida = [];
        for(let i = 0; i < redes_saidas; i++) {
            camada_saida[i] = 0;
        }

        neuronios.push(camada_saida);

        resolve(neuronios);
    });
    
    
    return await rede;
}

const reLU = async (neuronio, pesos) => {
    const total_calculado = new Promise((resolve, reject) => {
        let total = 0;

        for(let i=0; i< neuronio.length; i++) {
            
            total += neuronio[i]*pesos[i];
        }

        if(total <= 0) {
            resolve(0.01);
        }

        resolve(parseFloat((total/1000).toFixed(0)));
    });

    
    return await total_calculado;
}

const convert_file_to_string = async (file) => {
    const text = new Promise((resolve, reject) => {
        const new_text = file.toString('utf8');
        resolve(new_text);
    });

    return await text;
}

const load_file = async (path_file) => {
    return await read_file(path_file);
}

const create_data = async (path_file) => {
    const text = await load_file(path_file);
    
    const linhas = text.split('\n');
 
    const build_matrix = new Promise( (resolve, reject) => {
        const matrix_temp = [];
        let i = 0;
      
        linhas.forEach(linha => {
            const linha_array = linha.split(',');
            
            const obj = {
                data: linha_array[0].replace(/\./g, "-"),
                open: parseFloat(linha_array[1]),
                high: parseFloat(linha_array[2]),
                low: parseFloat(linha_array[3]),
                close: parseFloat(linha_array[4]),
                tick: parseInt(linha_array[5]),
                volume: parseInt(linha_array[6])
            };
            
            if(i > 0) {
                let data_array = obj.data.split(' ');

                if(data_array.length >= 2) {
                    let data = data_array[0].split('-');
                    let hora = data_array[1].split(':');

                    obj.data_obj = {
                        data_completa: data_array[0],
                        hora_completa: data_array[1],
                        dia: data[2],
                        mes: data[1],
                        ano: data[0],
                        hora: hora[0],
                        min: hora[1] 
                    };    
                }
            }
            
            matrix_temp[i] = obj;
            i++;
        });

        resolve(matrix_temp);
    });

    return await build_matrix;
}

const read_file = async (path_file) => {
    const file = new Promise((resolve, reject) => {
        fs.readFile(path_file, async (err, data) => {
            const text = await convert_file_to_string(data);
            resolve(text);
        });
    });

    return await file;
}

module.exports = {
    build_neuronios,
    reLU,
    create_data
}
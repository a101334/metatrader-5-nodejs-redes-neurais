#property copyright "Copyright 2020, Alex Rafael da Costa"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Includes                                                         |
//+------------------------------------------------------------------+
#include <Trade\Trade.mqh>
//+------------------------------------------------------------------+
//| Inputs                                                           |
//+------------------------------------------------------------------+
input int qtd_lote = 1;
int lote = qtd_lote;
input ENUM_TIMEFRAMES periodo_timeframe = PERIOD_M5;  
input ENUM_TIMEFRAMES periodo_timeframe_dia = PERIOD_D1;
int periodo_candle = 6;
int qtd_neuronio_entrada_adicional = 3;
input double desvio_padrao = 2;

input int hora_inicio_normal = 9;
input int minuto_inicio_normal = 0;
input int hora_fechamento_normal = 17; 
input int minuto_fechamento_normal = 55;

input int hora_maxima_abrir_posicao = 17;
input int minuto_maximo_abrir_posicao = 50;

input int hora_fechamento_pregao = 17;
input int minuto_fechamento_pregao = 50;

input int dia_min_operar = 1;
input int dia_max_operar = 31;

input bool trabalhar_segunda = true;
input bool trabalhar_terca = true;
input bool trabalhar_quarta = true;
input bool trabalhar_quinta = true;
input bool trabalhar_sexta = true;

input bool trabalhar_janeiro = true;
input bool trabalhar_fevereiro = true;
input bool trabalhar_marco = true;
input bool trabalhar_abril = true;
input bool trabalhar_maio = true;
input bool trabalhar_junho = true;
input bool trabalhar_julho = true;
input bool trabalhar_agosto = true;
input bool trabalhar_setembro = true;
input bool trabalhar_outubro = true;
input bool trabalhar_novembro = true;
input bool trabalhar_dezembro = true;

input double perda_maxima_dia_por_contrato = -3000;
input double fator_ganho_maximo_dia_por_contrato = 2;
input bool utilizar_lote_dinamico = false;
input double maximo_rebaixamento_da_estrategia = 600;
input int min_qtd_bars = 1;
input int max_qtd_bars_posicao = 94;

int qtd_n_entradas = ((periodo_candle - 1) - qtd_neuronio_entrada_adicional)*2;
int qtd_n_rede_camada_escondida_1 = (int)NormalizeDouble(qtd_n_entradas*2, 0);
int qtd_n_rede_camada_escondida_2 = (int)NormalizeDouble(qtd_n_entradas*2, 0);
int redes_saidas = 13;

double entrada[][17] = {{936,1770,-197,48,-934,988,325,137,-251,-120,758,-627,462,-1177,646,253,858},{-523,-43,486,-458,-258,-81,235,655,68,626,619,862,490,681,-899,911,483},{160,989,277,-627,795,-8,-1144,-317,-704,543,-408,764,-548,346,316,905,-778},{-25,428,1190,1040,-790,-297,124,246,204,541,-588,-725,1291,312,196,669,-577},{866,-361,-75,733,59,884,-288,899,-882,1016,588,-262,-451,-506,-401,-341,-1131},{-414,366,-800,-558,1028,75,-425,-1079,-724,-860,-457,372,-755,239,-1118,-751,-339},{-372,-501,441,218,650,297,-739,-747,-144,603,-289,458,-770,119,-180,66,249},{396,-845,11,105,-368,-1277,621,1054,857,-54,179,776,950,-448,-470,654,-695},{-702,-286,-501,-315,-783,827,947,281,534,-1007,-1147,-1031,-563,-514,797,743,-1039},{-369,829,-934,-374,363,193,655,-71,-553,620,-1155,-821,-788,-1166,248,-708,959},{71,-676,1054,-742,485,-1551,-309,158,1392,-638,825,-883,-210,-175,-319,-247,268},{-393,-33,-18,38,22,-565,136,1315,126,-898,686,-819,-24,-41,181,340,-878},{882,-629,-222,572,-1062,980,749,-3,-129,72,742,-72,-262,-960,-1159,616,-733},{-364,584,-1307,576,723,-137,-853,-608,886,-1005,1181,-87,-151,-631,296,201,213},{-498,844,-546,-541,-692,-504,-119,988,359,-287,-320,403,954,739,-488,-755,-828},{-1189,-991,-1036,628,-724,963,-460,244,-1030,960,269,723,348,-450,-604,-917,97},{-335,-714,295,-1003,-805,-250,1142,-539,-1011,-103,-606,-468,524,-1022,962,-332,1240},{190,-546,162,-980,-1441,456,427,-33,-255,-267,-373,-347,-561,-1178,684,657,379},{800,632,78,700,505,1759,1100,678,1646,1621,134,-321,328,-926,63,-35,53},{896,-793,-426,387,661,-1049,505,909,-436,-771,-341,166,1117,-545,-512,852,769},{-246,-645,-1041,982,-1080,-468,63,-34,404,-194,-792,-365,-797,632,167,88,83},{-1046,-179,53,261,277,295,218,-458,1003,-68,-774,-725,-208,-507,264,-515,119},{-997,224,622,172,-309,-443,39,81,-1125,-1204,879,326,54,-1390,677,-1059,715},{-137,-943,-326,453,-842,-335,-661,-73,-392,-120,248,303,-167,244,-414,129,-225},{-951,291,319,1001,275,-1968,247,-146,490,653,-849,-32,-424,366,25,-372,-167},{87,948,1065,-1524,324,146,-348,-137,-324,-78,311,131,1201,-749,559,-625,999},{342,462,664,-486,120,-402,-378,605,-534,-436,-792,970,804,406,952,565,517},{465,1138,1029,967,2,671,167,449,805,606,-601,1012,-332,684,-591,533,-510},{859,-182,-1592,58,-1071,394,-601,-297,142,-965,352,371,-275,205,-542,-1003,-478},{-943,669,357,616,478,660,-514,238,39,-1032,-609,75,530,-704,-103,690,-443},{-706,327,-664,-848,-605,-847,-503,521,-264,-631,724,-1007,458,-688,360,-39,-546},{-910,57,-534,-166,46,-241,-431,207,796,-465,-759,315,107,-489,-892,897,604},{728,170,-349,730,-800,165,584,171,-839,-77,393,1519,580,38,215,-249,333}};
double camada_1[][33] = {{811,-545,-999,-410,-211,774,-455,-92,-929,416,-526,799,810,-389,206,1131,575,320,-920,-118,-854,-808,-316,925,-1454,-251,475,-707,-192,1099,-111,710,-1305},{1065,1003,797,-4,914,847,-464,-890,-74,-206,409,172,163,114,-351,-12,984,137,-247,228,-1016,-185,960,-235,268,780,109,37,612,324,-664,86,657},{-633,-839,-1183,-1118,-124,49,-821,-463,543,-1014,437,-523,-49,732,21,382,-770,-109,720,213,781,365,-848,19,525,-303,405,-614,68,-787,-460,336,582},{-643,-404,1422,112,-444,-337,-419,671,431,877,-303,-25,-396,-548,-516,-1003,344,-112,401,430,44,680,-868,1155,-1114,376,-630,-206,-919,-926,-1003,-907,606},{1031,684,882,-664,1397,-132,-295,-247,-1329,-223,822,-484,-267,233,-1246,-310,-1307,701,458,176,-782,550,407,408,609,-105,-329,469,-550,886,803,-49,-510},{130,-267,500,-90,-445,525,-329,808,662,-405,721,-565,453,384,-453,-1146,317,383,-466,-34,893,-46,-522,772,255,376,743,1137,-701,-1264,31,-883,-672},{-809,904,-274,264,343,273,-165,-401,-241,557,-215,489,934,-689,-381,217,-624,484,989,804,653,1080,-637,-97,295,-1445,-537,-1526,941,-301,-754,466,57},{-250,-426,-241,1612,-956,-528,-1107,812,141,827,-119,183,767,-426,-510,-848,875,705,-856,-790,-530,-495,-326,469,192,176,307,453,87,-348,-307,1575,1345},{-451,-39,-111,189,879,484,446,134,244,783,-661,661,745,206,322,-116,-586,-791,124,-828,-767,-581,88,-86,342,1013,220,-890,-302,-1001,10,30,-271},{873,-156,-227,309,-809,-753,-398,-309,493,53,1310,740,43,290,-413,-762,43,213,830,-629,-498,-612,-734,112,513,-600,-82,3,-341,-1044,-883,136,-404},{-352,-283,708,-594,992,-904,-323,759,258,-627,467,-908,-312,-827,1609,-833,-283,996,-902,-192,131,-586,842,1499,100,102,-46,939,-585,-337,-167,-696,683},{-347,543,622,42,343,-154,81,-546,-319,35,507,424,45,205,-326,1336,45,-857,179,1711,872,685,250,-845,793,886,303,912,-290,-426,-126,-78,740},{508,-577,240,-667,891,756,-119,66,-717,-888,-595,-736,-554,-502,855,-921,-370,1271,692,265,381,-852,317,233,-531,328,-303,-727,-479,145,-8,-175,498},{-180,-1003,1465,-451,-583,393,769,104,-217,-196,403,-544,-549,-473,902,718,-320,-328,-546,-115,-385,-1158,-985,514,-1412,-93,-1001,1354,165,-458,-801,428,67},{-211,-845,5,-415,-365,531,-207,199,332,927,41,1111,-1064,1366,-222,672,393,663,-527,142,1191,558,491,181,-643,-522,-410,-1039,394,460,108,122,-233},{-154,575,785,1378,986,279,-989,-429,-114,-282,-943,-939,-1434,-724,-76,908,853,637,13,532,-394,380,357,-268,-725,-451,573,604,894,-144,1288,-742,-678},{711,907,608,-736,-205,493,-226,485,-279,-35,668,808,-706,49,54,489,666,-285,-237,-312,-283,311,775,-456,-1208,468,-611,465,-259,-181,-630,-1380,783},{457,-626,34,779,-734,-987,-983,-347,50,-782,-11,58,-219,1074,814,-18,167,956,177,1700,1284,342,1093,913,729,-705,349,1246,-883,-1315,306,605,-1268},{-526,88,-262,706,118,-88,796,-108,271,422,82,-49,-482,-1155,293,84,27,747,-241,795,361,73,768,-247,90,699,240,-463,288,-235,337,192,-496},{-30,322,473,-870,578,314,-241,-963,-409,-1790,-252,-439,284,259,361,-471,388,444,684,-95,-6,-487,1566,218,-307,172,699,236,18,307,533,216,549},{424,-215,746,6,-319,1430,834,-996,24,-672,708,-862,742,-22,813,-137,303,-544,-502,-727,-155,-264,541,109,839,-122,-929,-790,-879,-1166,1229,-855,204},{877,838,-532,524,-542,4,820,-469,681,474,-702,367,-1092,162,-431,-827,879,-575,288,-933,178,594,-356,1039,-112,-409,652,454,580,-104,623,452,-318},{1214,-1332,378,-481,666,84,506,-529,-536,-191,-497,-101,897,-460,960,630,-860,887,-692,316,-899,7,920,73,-282,-52,-307,969,938,-1060,463,-196,-756},{819,-882,381,-627,-353,-753,674,223,251,210,927,368,-386,-610,1131,747,383,-689,560,586,-809,-145,587,-444,-783,-606,-205,-614,-605,127,-444,-45,87},{222,-357,586,-189,-1403,752,-1053,-283,-486,-71,-451,220,688,878,62,145,-757,700,-288,651,13,66,-926,552,184,659,302,603,-316,710,224,-526,1055},{577,-720,-678,-1160,-396,-19,607,-824,1104,290,-979,-1020,-298,412,465,7,-114,631,300,491,601,-37,-664,321,411,-116,418,-243,-241,842,-584,30,-954},{-880,771,-740,262,-551,157,192,204,-150,-862,-638,1134,-695,-314,277,17,286,114,-129,294,860,-742,103,-159,-731,-714,-382,-450,-994,1189,-879,358,408},{-482,521,595,-900,-559,-111,-769,1053,566,-659,765,-670,288,-59,777,196,-379,-5,-576,-518,848,-952,941,403,-684,-10,-232,-913,133,-499,-701,-728,737},{619,-559,715,580,-1149,338,-539,486,-261,-614,-705,-1068,-430,-513,1199,180,-232,-851,123,-659,811,621,552,-424,894,136,632,449,-1044,-741,-261,285,124},{-1212,-561,886,641,715,-558,-565,722,448,-15,590,949,456,-145,552,528,1141,-1120,-878,635,-310,-694,694,50,-131,236,218,254,392,912,-648,-1272,-540},{680,-329,-1316,-553,-335,-458,-946,-487,1473,108,597,-6,958,726,775,-273,-130,907,-122,851,478,-841,92,-16,236,717,-693,-704,-428,-835,-881,429,-258},{679,216,532,300,-309,-198,392,417,-27,-870,651,-391,747,-621,-71,-42,1144,654,594,1444,-115,-710,889,357,269,261,1425,754,-60,-644,-101,484,-745},{804,228,276,-1305,1367,-279,705,-857,-403,-309,854,-12,-360,-526,-990,-499,-1212,-298,-780,-97,784,491,249,-541,-362,-170,-709,-461,-548,-855,695,-674,-430}};
double camada_2[][33] = {{1043,-362,747,974,215,-534,239,-325,195,401,-867,1219,-857,804,-271,476,-462,919,93,589,1040,103,18,532,-259,-337,-756,462,-710,-335,-730,-132,-295},{-823,762,479,-376,956,-317,-1610,479,-592,451,207,818,-268,-200,-713,1263,1129,-176,-86,686,-574,-221,-870,-399,1383,-638,-979,835,679,391,411,546,125},{-393,358,-299,-672,-539,-866,-515,-1099,-1001,-889,106,-931,41,-1271,176,-1032,282,-347,-488,-100,-473,440,238,11,-413,87,-25,-392,-160,674,424,538,-357},{86,-957,475,472,445,456,-471,-845,574,432,476,-958,326,852,-647,-1281,1336,1522,1218,51,965,-53,-605,-655,-347,-43,-847,-206,295,470,106,545,1265},{-836,-445,401,-868,1021,511,-828,-797,-1033,927,-127,163,624,-682,499,657,-594,263,-360,1157,-154,364,622,304,445,756,-77,580,-636,-1447,-224,586,-474},{-376,-234,252,842,681,828,342,-180,-1294,-1036,1744,-1226,-133,511,574,-1525,515,-553,-301,-838,303,-884,230,1032,82,497,-539,-237,-1103,-1219,-269,-593,-230},{660,856,-481,-152,-433,-1175,482,264,632,-583,-531,289,-349,81,733,105,508,498,-295,513,86,-470,-1091,427,1277,1075,99,828,-144,524,-874,-982,148},{136,-563,280,-1206,-188,-151,52,-1,-721,-364,-1390,-617,564,-269,776,-999,113,-802,-480,356,-637,316,-872,-443,1281,-1076,-708,237,160,-26,-819,605,27},{-616,698,-1169,-380,681,233,177,-749,371,-1036,-419,-67,738,-811,625,781,-190,-935,-1149,-815,-584,-273,-179,-340,-1102,918,-501,-1170,-76,185,-127,1037,-424},{-1264,-1165,-910,-921,-169,332,1,924,-297,396,972,744,95,-1291,-180,1125,61,730,-631,98,459,-266,220,-859,1095,137,-816,-1266,-361,-460,-289,-136,-648},{29,-233,203,-170,987,994,394,231,-281,22,-874,770,-294,510,-1127,951,-515,-1507,194,856,346,305,1147,968,-1357,-371,-4,437,328,-313,-498,926,608},{538,1143,1277,-435,-31,633,-114,629,-79,967,748,-861,374,-160,826,-388,-397,169,-55,-245,-124,842,388,547,-356,-64,-24,1071,-310,-504,-687,-821,-893},{382,620,719,437,1451,30,1258,648,-418,-645,642,-766,1037,250,-163,987,48,-184,-548,-327,-424,-488,550,376,-101,454,-645,1105,-464,54,-763,544,690},{-409,260,679,602,-1469,29,740,-138,-926,229,-778,-638,-119,-333,-332,-700,-345,397,35,458,462,-623,-861,-710,201,367,-205,-493,447,952,631,1088,-963}};

double neuronio_entrada[][17] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1};
double neuronio_camada_1[][33] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1};
double neuronio_camada_2[][33] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1};
double neuronio_saida[][13] = {0,0,0,0,0,0,0,0,0,0,0,0,0};
//+------------------------------------------------------------------+
//| Enums                                                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Variaveis globais                                                |
//+------------------------------------------------------------------+
CTrade trade;

double price_open [];
double price_high [];
double price_low [];
double price_close [];

double price_open_dia [];
double price_high_dia [];
double price_low_dia [];
double price_close_dia [];

bool comprado = false;
bool vendido = false;
ulong tick_comprado = 0;
ulong tick_vendido = 0;

ulong magic_number = 12345;

int qtd_bars_dia = 0;
int qtd_gains_dia = 0;
int qtd_loss_dia = 0;
int posicao_maxima_dia = 0;
int posicao_minima_dia = 0;

double maxima_dia = 0;
double minima_dia = 0;

double perda_maxima_dia = lote*perda_maxima_dia_por_contrato;
double ganho_maxima_dia = -1*lote*perda_maxima_dia_por_contrato*fator_ganho_maximo_dia_por_contrato;
double tamanho_medio_candles = 0;
double stop_loss_atual_pontos = 0;
double stop_gain_atual_pontos = 0;

double tick_size = 0;

MqlDateTime hora_atual, hora_servidor, hora_ajuste_qtd_bars_dia;

int dia_atual = 0;
int mes_atual = 0;
int dia_semana = -1;
double balanco_atual = 0;

string hl_stop_loss_atual = "hl_stop_loss_atual";
string hl_stop_gain_atual = "hl_gain_loss_atual";

string hl_suporte_atual = "hl_suporte_atual";
string hl_resistencia_atual = "hl_resistencia_atual";
string hl_suporte_anterior = "hl_suporte_anterior";
string hl_resistencia_anterior = "hl_resistencia_anterior";

int OnInit()
  {
//---
   remove_all_indicator();
   
   if(!inicializar_indicadores()){
      Print("Erro ao inicializar o sistema");
      return(INIT_FAILED);
   }
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   remove_all_indicator();
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
//---
   reset_valores_dia();
   
   if(verifica_posicao()) {
      hora_maxima_negociacao_zera_posicao();
   }
   
   if(is_new_bar()) {

      qtd_bars_dia ++;

      if(get_data()){
         calcula_tamanho_medio_candles();
         
         show_comentarios();
         
         if(!hora_negociacao()) {
            return;
         }
         
         if(verifica_posicao()) {
            
            if(!comprado && !vendido) {
               if(!logica_trade()) {
                  Print("Error: logica_trade");
               }
            } else {
               zerar_posicao();
            }
            
         }
      }
   } 
}
//+------------------------------------------------------------------+

bool is_new_bar(){
//--- memorize the time of opening of the last bar in the static variable
   static datetime last_time=0;
//--- current time
   datetime lastbar_time=(datetime)SeriesInfoInteger(_Symbol, periodo_timeframe, SERIES_LASTBAR_DATE);

//--- if it is the first call of the function
   if(last_time==0)
     {
      //--- set the time and exit
      last_time=lastbar_time;
      return(false);
     }

//--- if the time differs
   if(last_time!=lastbar_time)
     {
      //--- memorize the time and return true
      last_time=lastbar_time;
      return(true);
     }
//--- if we passed to this line, then the bar is not new; return false
   return(false);
}

bool inicializar_indicadores () {
   TimeToStruct(TimeCurrent(), hora_servidor);
   string hora_servidor_string = (string)hora_servidor.day+"/"+(string)hora_servidor.mon+"/"+(string)hora_servidor.year+" - "+(string)hora_servidor.hour+":"+(string)hora_servidor.min;
   
   Print("Hora Servidor: ", hora_servidor_string);
   
   tick_size = SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_SIZE);
   
   if(tick_size <= 0){
      Print("Error ao carregar a informação do tick_size");
      return false;
   }
   
   if(!ArraySetAsSeries(price_open, true)){
      Print("Error ao inverter o array price_open");
      return false;
   }
   
   if(!ArraySetAsSeries(price_high, true)){
      Print("Error ao inverter o array price_high");
      return false;
   }
   
   if(!ArraySetAsSeries(price_low, true)){
      Print("Error ao inverter o array price_low");
      return false;
   }
   
   if(!ArraySetAsSeries(price_close, true)){
      Print("Error ao inverter o array price_close");
      return false;
   }
   
   //candle dia
   if(!ArraySetAsSeries(price_open_dia, true)){
      Print("Error ao inverter o array price_open_dia");
      return false;
   }
   
   if(!ArraySetAsSeries(price_high_dia, true)){
      Print("Error ao inverter o array price_high_dia");
      return false;
   }
   
   if(!ArraySetAsSeries(price_low_dia, true)){
      Print("Error ao inverter o array price_low_dia");
      return false;
   }
   
   if(!ArraySetAsSeries(price_close_dia, true)){
      Print("Error ao inverter o array price_close_dia");
      return false;
   }
   
   trade.SetExpertMagicNumber(magic_number);
   
   if( !plotar_todas_linhas() ) {
      return false;
   }
   
   return true;
}

bool get_data() {
   int price_open_copy = CopyOpen(_Symbol, periodo_timeframe, 0, periodo_candle, price_open);
   
   if(price_open_copy != periodo_candle){
      Print("Error capturar os dados do price_open");
      return false;
   }
   
   int price_high_copy = CopyHigh(_Symbol, periodo_timeframe, 0, periodo_candle, price_high);
   
   if(price_high_copy != periodo_candle){
      Print("Error capturar os dados do price_high");
      return false;
   }
   
   int price_low_copy = CopyLow(_Symbol, periodo_timeframe, 0, periodo_candle, price_low);
   
   if(price_low_copy != periodo_candle){
      Print("Error capturar os dados do price_low");
      return false;
   }
   
   int price_close_copy = CopyClose(_Symbol, periodo_timeframe, 0, periodo_candle, price_close);
   
   if(price_close_copy != periodo_candle){
      Print("Error capturar os dados do price_close");
      return false;
   }
   
   //candles dia
   int price_open_dia_copy = CopyOpen(_Symbol, periodo_timeframe_dia, 0, periodo_candle, price_open_dia);
   
   if(price_open_dia_copy != periodo_candle){
      Print("Error capturar os dados do price_open_dia_");
      return false;
   }
   
   int price_high_dia_copy = CopyHigh(_Symbol, periodo_timeframe_dia, 0, periodo_candle, price_high_dia);
   
   if(price_high_dia_copy != periodo_candle){
      Print("Error capturar os dados do price_high_dia");
      return false;
   }
   
   int price_low_dia_copy = CopyLow(_Symbol, periodo_timeframe_dia, 0, periodo_candle, price_low_dia);
   
   if(price_low_dia_copy != periodo_candle){
      Print("Error capturar os dados do price_low_dia");
      return false;
   }
   
   int price_close_dia_copy = CopyClose(_Symbol, periodo_timeframe_dia, 0, periodo_candle, price_close_dia);
   
   if(price_close_dia_copy != periodo_candle){
      Print("Error capturar os dados do price_close_dia");
      return false;
   }
   
   return true;
}

bool verifica_posicao (){
   comprado = false;
   vendido = false;
   tick_comprado = 0;
   tick_vendido = 0;
   
   if(PositionSelect(_Symbol)) {
      if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY) {
         tick_comprado = PositionGetInteger(POSITION_TICKET);
         comprado = true;
      }
      
      if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL) {
         tick_vendido = PositionGetInteger(POSITION_TICKET);
         vendido = true;
      }
   }
  
   return true;
}

bool se_posicionar () {
   
   TimeToStruct(TimeCurrent(), hora_ajuste_qtd_bars_dia);
   
   if(hora_ajuste_qtd_bars_dia.hour == 9 && hora_ajuste_qtd_bars_dia.min == 5) {
      qtd_bars_dia = 1;
   }
   
   if(balanco_atual >= ganho_maxima_dia) {
      return true;
   }
   
   if(balanco_atual <= perda_maxima_dia) {
      return true;
   }
   
   if(hora_maxima_negociacao ()) {
      return true;
   }
   
   if(tempo_maximo_abrir_posicao()) {
      return true;
   }
   
   if(comprado || vendido) {
      return true;
   }
   
   if(qtd_bars_dia <= min_qtd_bars) {
      return true;
   }
   
   if(qtd_bars_dia >= max_qtd_bars_posicao) {
      return true;
   }
   
   reset_posicao();
   
   /*Print("neuronio_entrada[0][i]: ", DoubleToString(neuronio_entrada[0][0], 0));
   Print("neuronio_camada_1[0][i]: ", DoubleToString(neuronio_camada_1[0][0], 0));
   Print("neuronio_camada_2[0][i]: ", DoubleToString(neuronio_camada_2[0][0], 0));
   Print("neuronio_saida[0][i]: ", DoubleToString(neuronio_saida[0][0], 0));*/
   
   /*Print("entrada[0][i]: ", DoubleToString(entrada[1][0], 0));
   Print("camada_1[0][i]: ", DoubleToString(camada_1[1][0], 0));
   Print("camada_2[0][i]: ", DoubleToString(camada_2[1][0], 0));*/
   
   /*Print("size neuronio_entrada: ", IntegerToString((long)ArraySize(neuronio_entrada)));
   Print("size neuronio_camada_1: ", IntegerToString((long)ArraySize(neuronio_camada_1)));
   Print("size neuronio_camada_2: ", IntegerToString((long)ArraySize(neuronio_camada_2)));
   Print("size neuronio_saida: ", IntegerToString((long)ArraySize(neuronio_saida)));
   
   Print("size entrada: ", IntegerToString((long)ArraySize(entrada)/25));
   Print("size camada_1: ", IntegerToString((long)ArraySize(camada_1)/49));
   Print("size camada_2: ", IntegerToString((long)ArraySize(camada_2)/49));*/
   
   for(int i=0, count = 1; i < ArraySize(neuronio_entrada) - 1 - qtd_neuronio_entrada_adicional; i++) {
        if(count >= periodo_candle - 1) {
            count = 1;
        }

        if(count <= 1) {
            neuronio_entrada[0][i] = price_close[count] - price_open_dia[0];
            i++;
            neuronio_entrada[0][i] = price_close[count] - price_open_dia[0];
        } else {
            neuronio_entrada[0][i] = price_close[count] - price_open_dia[0];
            i++;
            
            double distancia_entre_candle = (price_close[1] - price_close[count])*(price_close[1] - price_close[count]) + (count - 1)*(count - 1);
            neuronio_entrada[0][i] = pow( distancia_entre_candle , 0.5);
        }

        count ++;
    }
    
    int index_neuronio_entrada = ArraySize(neuronio_entrada) - 1 - qtd_neuronio_entrada_adicional + 1;
    neuronio_entrada[0][index_neuronio_entrada] = qtd_bars_dia;
    
    index_neuronio_entrada ++;
    
    neuronio_entrada[0][index_neuronio_entrada] = dia_semana;
    
    index_neuronio_entrada ++;
    
    neuronio_entrada[0][index_neuronio_entrada] = mes_atual;
    
    if(qtd_bars_dia == 2) {
      Print("qtd_bars: ", qtd_bars_dia, " - price_close[1] : ", DoubleToString(price_close[1], 3), " - price_close[2] : ", DoubleToString(price_close[2], 3), " - price_close[3] : ", DoubleToString(price_close[3], 3) );
    }
    
    
    Print("============== neuronio_entrada =====================");
    Print("qtd_bars: ", qtd_bars_dia, " - price_close[1] : ", DoubleToString(price_close[1], 3), " - price_close[2] : ", DoubleToString(price_close[2], 3), " - price_close[3] : ", DoubleToString(price_close[3], 3) );
    ArrayPrint(neuronio_entrada);
    Print("=====================================================");
    
    for(int l=0; l < ArraySize(neuronio_camada_1); l++ ) {
        //neuronio_camada_1[0][l] = reLU(neuronio_entrada, entrada, l);
        
        double total = 0;

        for(int i=0; i< ArraySize(neuronio_entrada); i++) {
            total += neuronio_entrada[0][i]*entrada[l][i];
        }
      
        if(total <= 0) {
            neuronio_camada_1[0][l] = 0.01;
            continue;
        }
      
        neuronio_camada_1[0][l] = NormalizeDouble((total/1000), 3);
    }
    
    Print("============== neuronio_camada_1 =====================");
    ArrayPrint(neuronio_camada_1);
    Print("=====================================================");
    
    for(int k=0; k < ArraySize(neuronio_camada_2); k++ ) {
        //neuronio_camada_2[0][k] = reLU(neuronio_camada_1, camada_1, k);
        
        double total = 0;

        for(int i=0; i< ArraySize(neuronio_camada_1); i++) {
            total += neuronio_camada_1[0][i]*camada_1[k][i];
        }
      
        if(total <= 0) {
            neuronio_camada_2[0][k] = 0.01;
            continue;
        }
      
        neuronio_camada_2[0][k] = NormalizeDouble((total/1000), 3);
    }
    
    Print("============== neuronio_camada_2 =====================");
    ArrayPrint(neuronio_camada_2);
    Print("=====================================================");
    
    for(int m=0; m < ArraySize(neuronio_saida); m++ ) {
        //neuronio_saida[0][m] = reLU(neuronio_camada_2, camada_2, m);
        
        double total = 0;

        for(int i=0; i< ArraySize(neuronio_camada_2); i++) {
            total += neuronio_camada_2[0][i]*camada_2[m][i];
        }
      
        if(total <= 0) {
            neuronio_saida[0][m] = 0.01;
            continue;
        }
      
        neuronio_saida[0][m] = NormalizeDouble((total/1000), 3);
    }
    
    Print("============== neuronio_saida =====================");
    ArrayPrint(neuronio_saida);
    Print("=====================================================");
    
    int id_saida = 0;
     double maior_valor = neuronio_saida[0][0];

     for(int i = 0; i < ArraySize(neuronio_saida); i++) {
         if(maior_valor <= neuronio_saida[0][i]) {
             id_saida = i;
             maior_valor = neuronio_saida[0][i];
         }
     }
    
    //Print("id_saida: ", id_saida," - qtd_bars_dia: ", qtd_bars_dia);
    
    switch(id_saida) {
    
        case 0: {
            se_posicionar_comprado(tamanho_medio_candles, price_low[1] - (price_high[1] - price_low[1]) , price_close[1] + (price_close[1] - price_low[1])*2); 
        } break;

        case 1: {
            se_posicionar_comprado(tamanho_medio_candles, price_low[1] - (price_high[1] - price_low[1])*2 , price_close[1] + (price_close[1] - price_low[1])*2); 
        } break;

        case 2: {
            se_posicionar_comprado(tamanho_medio_candles, price_low[1] - (price_high[1] - price_low[1])*3 , price_close[1] + (price_close[1] - price_low[1])*2); 
        } break;

        case 3: {
            se_posicionar_comprado(tamanho_medio_candles, price_low[1] - (price_high[1] - price_low[1]) , price_close[1] + (price_close[1] - price_low[1])*4); 
        } break;

        case 4: {
            se_posicionar_comprado(tamanho_medio_candles, price_low[1] - (price_high[1] - price_low[1])*2 , price_close[1] + (price_close[1] - price_low[1])*4); 
        } break;

        case 5: {
            se_posicionar_comprado(tamanho_medio_candles, price_low[1] - (price_high[1] - price_low[1])*3 , price_close[1] + (price_close[1] - price_low[1])*4); 
        } break;

        case 6: {
            se_posicionar_vendido(tamanho_medio_candles, price_high[1] + (price_high[1] - price_low[1]), price_close[1] - (price_high[1] - price_low[1])*2);
        } break;

        case 7: {
            se_posicionar_vendido(tamanho_medio_candles, price_high[1] + (price_high[1] - price_low[1])*2, price_close[1] - (price_high[1] - price_low[1])*2);
        } break;

        case 8: {
            se_posicionar_vendido(tamanho_medio_candles, price_high[1] + (price_high[1] - price_low[1])*3, price_close[1] - (price_high[1] - price_low[1])*2);
        } break;

        case 9: {
            se_posicionar_vendido(tamanho_medio_candles, price_high[1] + (price_high[1] - price_low[1]), price_close[1] - (price_high[1] - price_low[1])*4);
        } break;

        case 10: {
            se_posicionar_vendido(tamanho_medio_candles, price_high[1] + (price_high[1] - price_low[1])*2, price_close[1] - (price_high[1] - price_low[1])*4);
        } break;

        case 11: {
            se_posicionar_vendido(tamanho_medio_candles, price_high[1] + (price_high[1] - price_low[1])*3, price_close[1] - (price_high[1] - price_low[1])*4);
        } break;

        case 12: {
            //console.log("Calma, observe mais !!!!!")
        } break;
    }
   
   return true;
}

bool logica_trade() {
   if(comprado || vendido) {
   
   } else {
      if(!se_posicionar ()){
         Print("Error: se_posicionar");
         return false;
      }
   }
   
   return true;
}

void reset_valores_dia() {
   TimeToStruct(TimeCurrent(), hora_atual);
   
   if(dia_atual != hora_atual.day) {
      dia_atual = hora_atual.day;
      dia_semana = hora_atual.day_of_week;
      mes_atual = hora_atual.mon;
      comprado = false;
      vendido = false;
      tick_comprado = 0;
      tick_vendido = 0;
      qtd_bars_dia = 0;
      stop_loss_atual_pontos = 0;
      stop_gain_atual_pontos = 0;
     
      qtd_gains_dia = 0;
      qtd_loss_dia = 0;
      
      balanco_atual = 0;
      
      reset_posicao();
      
      if(mes_atual != hora_atual.mon && utilizar_lote_dinamico) {
         
         if( AccountInfoDouble(ACCOUNT_BALANCE) <= 25000 ) {
            lote = (int)NormalizeDouble(AccountInfoDouble(ACCOUNT_BALANCE)/maximo_rebaixamento_da_estrategia, 0);
         }
         
         if( AccountInfoDouble(ACCOUNT_BALANCE) > 25000 && AccountInfoDouble(ACCOUNT_BALANCE) <= 50000 ) {
            lote = (int)NormalizeDouble(AccountInfoDouble(ACCOUNT_BALANCE)/(maximo_rebaixamento_da_estrategia + 500), 0);
         }
         
         if( AccountInfoDouble(ACCOUNT_BALANCE) > 50000 && AccountInfoDouble(ACCOUNT_BALANCE) <= 100000 ) {
            lote = (int)NormalizeDouble(AccountInfoDouble(ACCOUNT_BALANCE)/(maximo_rebaixamento_da_estrategia + 1000), 0);
         }
         
         if( AccountInfoDouble(ACCOUNT_BALANCE) > 100000 && AccountInfoDouble(ACCOUNT_BALANCE) <= 200000 ) {
            lote = (int)NormalizeDouble(AccountInfoDouble(ACCOUNT_BALANCE)/(maximo_rebaixamento_da_estrategia + 1500), 0);
         }
         
         if( AccountInfoDouble(ACCOUNT_BALANCE) > 250000 ) {
            lote = (int)NormalizeDouble(AccountInfoDouble(ACCOUNT_BALANCE)/(maximo_rebaixamento_da_estrategia*3), 0);
         }
         
         if(lote >= 500) {
            lote = 500;
         }
         
         perda_maxima_dia = lote*perda_maxima_dia_por_contrato;
         ganho_maxima_dia = -1*lote*perda_maxima_dia_por_contrato*fator_ganho_maximo_dia_por_contrato;
      }
   }
   
}

double normalize_pontos (double pontos) {
   return NormalizeDouble(pontos/tick_size, 0)*tick_size;
}

bool hora_negociacao () {
    TimeToStruct(TimeCurrent(), hora_atual);
    
    if (hora_atual.day_of_week == 0 || hora_atual.day_of_week == 6) {
      return false;
    }
    
    //Meses do ano
    if(hora_atual.mon == 1 && !trabalhar_janeiro) {
      return false;
    }
   
    if(hora_atual.mon == 2 && !trabalhar_fevereiro) {
      return false;
    }

    if(hora_atual.mon == 3 && !trabalhar_marco) {
      return false;
    }
    
    if(hora_atual.mon == 4 && !trabalhar_abril) {
      return false;
    }
    
    if(hora_atual.mon == 5 && !trabalhar_maio) {
      return false;
    }
    
    if(hora_atual.mon == 6 && !trabalhar_junho) {
      return false;
    }
    
    if(hora_atual.mon == 7 && !trabalhar_julho) {
      return false;
    }
    
    if(hora_atual.mon == 8 && !trabalhar_agosto) {
      return false;
    }
    
    if(hora_atual.mon == 9 && !trabalhar_setembro) {
      return false;
    }
    
    if(hora_atual.mon == 10 && !trabalhar_outubro) {
      return false;
    }
    
    if(hora_atual.mon == 11 && !trabalhar_novembro) {
      return false;
    }
    
    if(hora_atual.mon == 12 && !trabalhar_dezembro) {
      return false;
    }
    
    //Dias da Semana
    if(hora_atual.day_of_week == 1 && !trabalhar_segunda) {
      return false;
    }
    
    if(hora_atual.day_of_week == 2 && !trabalhar_terca) {
      return false;
    }
    
    if(hora_atual.day_of_week == 3 && !trabalhar_quarta) {
      return false;
    }
    
    if(hora_atual.day_of_week == 4 && !trabalhar_quinta) {
      return false;
    }
    
    if(hora_atual.day_of_week == 5 && !trabalhar_sexta) {
      return false;
    }
    
    //Intervalo de dias do mês
    if(hora_atual.day < dia_min_operar) {
      return false;
    }
    
    if(hora_atual.day > dia_max_operar) {
      return false;
    }
    
    if (hora_atual.hour >= hora_inicio_normal && hora_atual.hour <= hora_fechamento_normal) {
    
       if (hora_atual.hour == hora_inicio_normal) {
         
         if (hora_atual.min >= minuto_inicio_normal) {
            return true;
         }
         
         return false;
       }
       
        if (hora_atual.hour == hora_fechamento_normal) {
         
         if (hora_atual.min <= minuto_fechamento_normal) {
            return true;
         }
         
         return false;
       }
       
       return true;
    }
    
    return false;
}

bool hora_maxima_negociacao () {
    TimeToStruct(TimeCurrent(), hora_atual);
    
    if(hora_atual.hour > hora_fechamento_pregao) {
      return true;
    }
    
    if (hora_atual.hour >= hora_fechamento_pregao && hora_atual.min >= minuto_fechamento_pregao) {
      return true;
    }
    
    if(qtd_bars_dia >= max_qtd_bars_posicao) {
      return true;
    }
    
    return false;
}

bool tempo_maximo_abrir_posicao () {
    TimeToStruct(TimeCurrent(), hora_atual);
    
    if (hora_atual.hour > hora_maxima_abrir_posicao) {
      return true;
    }
    
    if (hora_atual.hour >= hora_maxima_abrir_posicao && hora_atual.min >= minuto_maximo_abrir_posicao) {
      return true;
    }
    
    return false;
}

void hora_maxima_negociacao_zera_posicao() {

  if(hora_maxima_negociacao() && (comprado || vendido)) {
      if(!trade.PositionClose(_Symbol)){
         Print("Error: zerar posicao (hora_maxima_negociacao_zera_posicao)");
         return;
      }
      
      balanco_atual += PositionGetDouble(POSITION_PROFIT);
      Print("Balanço atual: "  ,DoubleToString(balanco_atual, 2));
      Print("posicionado -> zerado (hora_maxima_negociacao_zera_posicao)");
      Print("(hora_maxima) - profit: "  ,DoubleToString(PositionGetDouble(POSITION_PROFIT), 2), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
  }
}

void calcula_tamanho_medio_candles() {
   double soma_total_tamanho_candle = 0;
   
   for(int i = 0; i < periodo_candle; i++) {
      soma_total_tamanho_candle += price_high[i] - price_low[i];
   }
   
   tamanho_medio_candles = soma_total_tamanho_candle/periodo_candle; 
}

bool zerar_posicao() {
   
   if (comprado) {
      // 3° Axioma: Esperança (obs: perceber que o barco está afundado, abandonar logo)
      if(comprado && price_close[1] <= stop_loss_atual_pontos) {
          bool res_pos_close = trade.PositionClose(_Symbol);
          
          if(res_pos_close){
            
            if(trade.ResultRetcode() == 10008 || trade.ResultRetcode() == 10009) {
               balanco_atual += PositionGetDouble(POSITION_PROFIT);
               Print("Balanço atual: "  ,DoubleToString(balanco_atual, 2));
               Print("(comprado) - price_open[0]: ", DoubleToString(price_open[0],1), " - price_open[1]: ", DoubleToString(price_open[1],1) ," - price_close[0]: ", DoubleToString(price_close[0],1) ," - price_close[1]: ", DoubleToString(price_close[1],1) ," - profit: "  ,DoubleToString(PositionGetDouble(POSITION_PROFIT), 2), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
               qtd_loss_dia ++;
               return true;
            }
            
            Print("Error: trade.PositionClose2 - ResultRetcode: ", trade.ResultRetcode());
            return false;
          }
      }
      
      if(comprado && price_close[1] >= stop_gain_atual_pontos) {
          stop_loss_atual_pontos = price_low[1] - (price_high[1] - price_low[1]);
          stop_gain_atual_pontos = price_close[1] + (price_high[1] - price_low[1]);
          
          ObjectSetDouble(0, hl_stop_gain_atual, OBJPROP_PRICE, stop_gain_atual_pontos);
          ObjectSetDouble(0, hl_stop_loss_atual, OBJPROP_PRICE, stop_loss_atual_pontos);

          /*bool res_pos_close = trade.PositionClose(_Symbol);
          if(res_pos_close){

            if(trade.ResultRetcode() == 10008 || trade.ResultRetcode() == 10009) {
               balanco_atual += PositionGetDouble(POSITION_PROFIT);
               Print("Balanço atual: "  ,DoubleToString(balanco_atual, 2));
               Print("(comprado) - price_open[0]: ", DoubleToString(price_open[0],0), " - price_open[1]: ", DoubleToString(price_open[1],0) ," - price_close[0]: ", DoubleToString(price_close[0],0) ," - price_close[1]: ", DoubleToString(price_close[1],0) ," - profit: "  ,DoubleToString(PositionGetDouble(POSITION_PROFIT), 2), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
               qtd_gains_dia ++;
               return true;
            }
            
            Print("Error: trade.PositionClose2 - ResultRetcode: ", trade.ResultRetcode());
            return false;
          }*/
      }
   }
   
   if (vendido) {
      // 3° Axioma: Esperança (obs: perceber que o barco está afundado, abandonar logo)
      if(vendido && price_close[1] >= stop_loss_atual_pontos) {
          bool res_pos_close = trade.PositionClose(_Symbol);
          
          if(res_pos_close){
      
            if(trade.ResultRetcode() == 10008 || trade.ResultRetcode() == 10009) {
               balanco_atual += PositionGetDouble(POSITION_PROFIT);
               Print("Balanço atual: "  ,DoubleToString(balanco_atual, 2));
               Print("(vendido) - price_open[0]: ", DoubleToString(price_open[0],1), " - price_open[1]: ", DoubleToString(price_open[1],1) ," - price_close[0]: ", DoubleToString(price_close[0],1) ," - price_close[1]: ", DoubleToString(price_close[1],1) ," - profit: "  ,DoubleToString(PositionGetDouble(POSITION_PROFIT), 2), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
               qtd_loss_dia ++;
               return true;
            }
            
            Print("Error: trade.PositionClose2 - ResultRetcode: ", trade.ResultRetcode());
            return false;
          }
      }
      
      if(vendido && price_close[1] <= stop_gain_atual_pontos) {
            stop_loss_atual_pontos = price_high[1] + (price_high[1] - price_low[1]);
            stop_gain_atual_pontos = price_close[1] - (price_high[1] - price_low[1]);
            
            ObjectSetDouble(0, hl_stop_gain_atual, OBJPROP_PRICE, stop_gain_atual_pontos);
            ObjectSetDouble(0, hl_stop_loss_atual, OBJPROP_PRICE, stop_loss_atual_pontos);

          /*bool res_pos_close = trade.PositionClose(_Symbol);
          
          if(res_pos_close){
      
            if(trade.ResultRetcode() == 10008 || trade.ResultRetcode() == 10009) {
               balanco_atual += PositionGetDouble(POSITION_PROFIT);
               Print("Balanço atual: "  ,DoubleToString(balanco_atual, 2));
               Print("(vendido) - price_open[0]: ", DoubleToString(price_open[0],0), " - price_open[1]: ", DoubleToString(price_open[1],0) ," - price_close[0]: ", DoubleToString(price_close[0],0) ," - price_close[1]: ", DoubleToString(price_close[1],0) ," - profit: "  ,DoubleToString(PositionGetDouble(POSITION_PROFIT), 2), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
               qtd_gains_dia ++;
               return true;
            }
            
            Print("Error: trade.PositionClose2 - ResultRetcode: ", trade.ResultRetcode());
            return false;
          }*/
      }
   }
 
   return true;
}

void show_comentarios() {
   datetime time  = iTime(Symbol(),periodo_timeframe,0);
             
    Comment (
               "\n\n\n\n\n\n\n\n\n\n\nTime: "  ,TimeToString(time,TIME_MINUTES),"\n",
               "Current Position Profit: "  ,PositionGetDouble(POSITION_PROFIT),"\n",
               "Balanço atual: "  ,DoubleToString(balanco_atual, 2),"\n",
               "Stop Loss atual (pontos)", DoubleToString(stop_loss_atual_pontos, 2), "\n",
               "Stop Gain atual (pontos)", DoubleToString(stop_gain_atual_pontos, 2), "\n",
               "QTD Loss dia", qtd_loss_dia, "\n",
               "QTD Gain dia", qtd_gains_dia, "\n"
            );
}

void remove_all_indicator() {
   int subwindows=(int)ChartGetInteger(0,CHART_WINDOWS_TOTAL);

   for(int i=subwindows;i>=0;i--)
     {
      int indicators=ChartIndicatorsTotal(0,i);

      for(int j=indicators-1; j>=0; j--)
        {
         ChartIndicatorDelete(0,i,ChartIndicatorName(0,i,j));
        }
     }
}

bool plotar_linha_horizontal (string nome_objeto, double posicao_hl, color color_hl, ENUM_LINE_STYLE style_line, int width) { 
   
   if( !ObjectCreate(0, nome_objeto, OBJ_HLINE, 0, 0, posicao_hl) ){
      Print("Problema ao criar a linha horizontal: " + nome_objeto);
      return false;
   }
   
   if( !ObjectSetInteger(0, nome_objeto, OBJPROP_COLOR, color_hl) ){
      Print("Problema ao setar a cor linha horizontal: " + nome_objeto);
      return false;
   }
   
   if( !ObjectSetInteger(0, nome_objeto, OBJPROP_WIDTH, width) ){
      Print("Problema ao setar a espesura linha horizontal: " + nome_objeto);
      return false;
   }
   
   if( !ObjectSetInteger(0, nome_objeto, OBJPROP_STYLE, style_line) ){
      Print("Problema ao setar a espesura linha horizontal: " + nome_objeto);
      return false;
   }
   
   return true;
}

bool plotar_todas_linhas() {
   
   //Linhas Stop Loss, Stop Gain
   if( !plotar_linha_horizontal(hl_stop_loss_atual, 0, clrRed, STYLE_SOLID, 2) ) {
      Print("Problema ao criar a hl_stop_loss_atual");
      return false;
   }
   
   if( !plotar_linha_horizontal(hl_stop_gain_atual, 0, clrGreen, STYLE_SOLID, 2) ) {
      Print("Problema ao criar a hl_gain_loss_atual");
      return false;
   }
   
   //Linhas Suporte e Resistência.
   if( !plotar_linha_horizontal(hl_suporte_atual, 0, clrPink, STYLE_SOLID, 2) ) {
      Print("Problema ao criar a hl_suporte_atual");
      return false;
   }
   
   if( !plotar_linha_horizontal(hl_resistencia_atual, 0, clrPurple, STYLE_SOLID, 2) ) {
      Print("Problema ao criar a hl_resistencia_atual");
      return false;
   }
   
   if( !plotar_linha_horizontal(hl_suporte_anterior, 0, clrPink, STYLE_DASH, 2) ) {
      Print("Problema ao criar a hl_suporte_anterior");
      return false;
   }
   
   if( !plotar_linha_horizontal(hl_resistencia_anterior, 0, clrPurple, STYLE_DASH, 2) ) {
      Print("Problema ao criar a hl_resistencia_anterior");
      return false;
   }
   
   return true;
}

void reset_posicao() {
   stop_gain_atual_pontos = 0;
   stop_loss_atual_pontos = 0;
   
   ObjectSetDouble(0, hl_stop_gain_atual, OBJPROP_PRICE, stop_gain_atual_pontos);
   ObjectSetDouble(0, hl_stop_loss_atual, OBJPROP_PRICE, stop_loss_atual_pontos);
}


bool se_posicionar_comprado(double tamanho_posicao, double stop_loss_posicao, double stop_gain_posicao) {
   
   double stop_loss_ordem = stop_loss_posicao - tamanho_posicao*30;
   double stop_gain_ordem = stop_gain_posicao + tamanho_posicao*30;
   
   bool res_trade_buy = trade.Buy(lote, _Symbol, 0, normalize_pontos(stop_loss_ordem), normalize_pontos(stop_gain_ordem), "zerado->comprado");
   
   if(res_trade_buy){
   
      if(trade.ResultRetcode() == 10008 || trade.ResultRetcode() == 10009) {
    
         stop_loss_atual_pontos = stop_loss_posicao;
         stop_gain_atual_pontos = stop_gain_posicao;
         
         ObjectSetDouble(0, hl_stop_gain_atual, OBJPROP_PRICE, stop_gain_atual_pontos);
         ObjectSetDouble(0, hl_stop_loss_atual, OBJPROP_PRICE, stop_loss_atual_pontos);

         Print("(comprado) - price: ", DoubleToString(price_close[1], 1), " - stop_loss: ", DoubleToString(stop_loss_atual_pontos, 1), " - stop_gain: ", DoubleToString(stop_gain_atual_pontos, 1), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
         return true;
      }
      
      Print("Error: trade.buy - ResultRetcode: ", trade.ResultRetcode());
      return false;
   }
   
   Print("Error: trade.buy - antes de verificar o ResultRetcode");
   return false; 
}

bool se_posicionar_vendido(double tamanho_posicao, double stop_loss_posicao, double stop_gain_posicao) {
   
   double stop_loss_ordem = stop_loss_posicao + tamanho_posicao*30;
   double stop_gain_ordem = stop_gain_posicao - tamanho_posicao*30;
   
   bool res_trade_sell = trade.Sell(lote, _Symbol, 0, normalize_pontos(stop_loss_ordem), normalize_pontos(stop_gain_ordem), "zerado->vendido");
   
   if(res_trade_sell){
   
      if(trade.ResultRetcode() == 10008 || trade.ResultRetcode() == 10009) {
    
         stop_loss_atual_pontos = stop_loss_posicao;
         stop_gain_atual_pontos = stop_gain_posicao;
         
         ObjectSetDouble(0, hl_stop_gain_atual, OBJPROP_PRICE, stop_gain_atual_pontos);
         ObjectSetDouble(0, hl_stop_loss_atual, OBJPROP_PRICE, stop_loss_atual_pontos);
         
         Print("(vendido) - price: ", DoubleToString(price_close[1], 1), " - stop_loss: ", DoubleToString(stop_loss_atual_pontos, 1), " - stop_gain: ", DoubleToString(stop_gain_atual_pontos, 1), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
         return true;
      }
      
      Print("Error: trade.sell - ResultRetcode: ", trade.ResultRetcode());
      return false;
   }
   
   Print("Error: trade.sell - antes de verificar o ResultRetcode");
   return false; 
}


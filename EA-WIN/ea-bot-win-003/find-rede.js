var fs = require('fs');
var http = require('http');
var melhores_redes = require('./melhores-redes');
const { parametros_metatrader, parametros_rede_neural} = require('./variaveis-globais');
const { build_neuronios, reLU, create_data } = require('./funcoes-redes');

//var melhor_individuo_inicial = melhores_redes.geracao_1[0].pesos;
var melhor_individuo_inicial = [];
// var serie_dados = [];

var data_inicial_treino = parametros_metatrader.data_inicial_treino;
var data_final_treino = parametros_metatrader.data_final_treino;
var custo_por_trader = parametros_metatrader.custo_por_trader;
var desconto_por_trade = parametros_metatrader.desconto_por_trade;
var custo_servidor_cloud = parametros_metatrader.custo_servidor_cloud;

//==========Geracao
var qtd_populacao = 4;
var qtd_geracao = 0;
var qtd_max_geracao = 2;
var qtd_max_epoca = 50000;
var melhores_da_resultados = [];
var resultado_geracao = [];
//==================

var periodo_candles = parametros_metatrader.periodo_candles;
var qtd_neuronio_entrada_adicional = parametros_metatrader.qtd_neuronio_entrada_adicional;
var min_qtd_bars = parametros_metatrader.min_qtd_bars;
var max_qtd_bars_posicao = parametros_metatrader.max_qtd_bars_posicao;
var penalidade_por_erro_trade = parametros_metatrader.penalidade_por_erro_trade;

var qtd_bars = 0;
var dia_atual = "";
var dia_da_semana = "";
var dia_mes_do_ano = "";
var preco_abertura = 0;
var preco_max = 0;
var preco_min = 0;
var data_atual_debug = "";
var hora_atual_debug = "";

var price_open = [];
var price_high = [];
var price_low = [];
var price_close = [];

var maximo_atual = 0;
var minimo_atual = 0;

//Redes neurais
const qtd_n_entradas = parametros_rede_neural.qtd_n_entradas;
const qtd_n_rede_camada_escondida_1 = parametros_rede_neural.qtd_n_rede_camada_escondida_1;
const qtd_n_rede_camada_escondida_2 = parametros_rede_neural.qtd_n_rede_camada_escondida_2;
const redes_saidas = parametros_rede_neural.redes_saidas;

var limitar_valor_maximo_pesos = false;
var valor_maximo_pesos = 1000;
var variacao_pesos_gen_x = 1;
var porcentagem_caracteristica_melhor = 0.2; 

//========================

var i=0;
var j=0;

//Variaveis globais metatrader
var COMPRADO = false;
var VENDIDO = false;
var POSICAO = null;
var STOP_LOSS = null;
var STOP_GAIN = null;
var BALANCE = 0;
var QTD_ERROS = 0;
var QTD_ACERTOS = 0;
var LUCRO_BRUTO = 0;
var PERDA_BRUTA = 0;
var QTD_TRADES = 0;
var QTD_ACERTOS_COMPRADO = 0;
var QTD_ERROS_COMPRADO = 0;
var QTD_ACERTOS_VENDIDO = 0;
var QTD_ERROS_VENDIDO = 0;
var PENALIDADE_ERRO_TRADE = 0;

var text_log = "";

var redes_gravadas = [];

const start = async (melhor_individuo_geracao) => {
    text_log += `${data_inicial_treino} ate ${data_final_treino}\nperiodo_candle: ${periodo_candles}\nqtd_neuronio_entrada_adicional:${qtd_neuronio_entrada_adicional}\nqtd_n_entradas:${qtd_n_entradas}\nqtd_n_rede_camada_escondida_1:${qtd_n_rede_camada_escondida_1}\nqtd_n_rede_camada_escondida_2:${qtd_n_rede_camada_escondida_2}\nredes_saidas:${redes_saidas}\n`;

    this.text_log = "\n";
    
    initialize();

    // `../../series-historicas/WIN/WINNM5-treino-min.csv`
    // `../../series-historicas/WIN/WINNM5-treino.csv`
    // `../../series-historicas/WIN/WINNM5-treino-v3.csv`
    //  `../../series-historicas/WIN/WINNM5.csv`
    //  `../../series-historicas/WIN/WINNM5-validacao.csv`
    //  `../../series-historicas/WIN/WINNM5-validacao2.csv`

    const serie_dados = await create_data(`../../series-historicas/WIN/WINNM5-treino.csv`);
    //const serie_dados = await create_data(`../../series-historicas/WIN/WINNM5-treino-v3.csv`);
    let data_inicio_analise = new Date(data_inicial_treino);
    let data_fim_analise = new Date(data_final_treino);

    melhores_da_resultados = [];

    for(qtd_geracao = 0; qtd_geracao <= qtd_max_geracao; qtd_geracao++) {
        neuronios = [];
        populacao = await build_populacao(melhor_individuo_geracao);
        neuronios = await build_neuronios(qtd_n_entradas, qtd_n_rede_camada_escondida_1, qtd_n_rede_camada_escondida_2, redes_saidas);
        resultado_geracao = [];
        await run_populacao_day_trade(populacao, serie_dados, data_inicio_analise, data_fim_analise);

        i=0;
        j=0;
        //console.log(`Gen: ${qtd_geracao}`);
        text_log += `Gen: ${qtd_geracao}\n`;
        neuronios = [];
    }

    return melhores_da_resultados;
}

const run_day_trade = async (individuo, serie_dados, data_inicio_analise, data_fim_analise) => {

    const individuo_day_trade = new Promise( async (resolve, reject) => {
        for(let i=1; i < serie_dados.length; i++) {
            get_data(serie_dados[i]);
            
            if(!serie_dados[i].hasOwnProperty("data_obj")) {
                continue;
            }
            
            let data_atual = new Date(serie_dados[i].data_obj.data_completa);
            
            if( data_atual >= data_fim_analise || i >= serie_dados.length - 2) {
                let perda_bruta_fator_lucro = PERDA_BRUTA;
                if(perda_bruta_fator_lucro == 0) {
                    perda_bruta_fator_lucro = -0.1;
                }

                let obj = {
                    resultado: ( BALANCE - custo_servidor_cloud),
                    QTD_TRADES: QTD_TRADES,
                    QTD_ACERTOS: QTD_ACERTOS,
                    QTD_ERROS: QTD_ERROS,
                    ACERTOS: ((QTD_ACERTOS/QTD_TRADES)*100),
                    ERROS: ((QTD_ERROS/QTD_TRADES)*100),
                    LUCRO_LIQUIDO: (LUCRO_BRUTO + PERDA_BRUTA),
                    LUCRO_BRUTO: LUCRO_BRUTO,
                    PERDA_BRUTA: PERDA_BRUTA,
                    FATOR_LUCRO: (LUCRO_BRUTO/(perda_bruta_fator_lucro*(-1))),
                    QTD_ACERTOS_COMPRADO: QTD_ACERTOS_COMPRADO,
                    QTD_ERROS_COMPRADO: QTD_ERROS_COMPRADO,
                    ACERTOS_COMPRADO: (QTD_ACERTOS_COMPRADO/(QTD_ACERTOS_COMPRADO + QTD_ERROS_COMPRADO)),
                    QTD_ACERTOS_VENDIDO: QTD_ACERTOS_VENDIDO,
                    QTD_ERROS_VENDIDO: QTD_ERROS_VENDIDO,
                    ACERTOS_VENDIDO: (QTD_ACERTOS_VENDIDO/(QTD_ACERTOS_VENDIDO + QTD_ERROS_VENDIDO)),
                    PENALIDADE_ERRO_TRADE: PENALIDADE_ERRO_TRADE,
                    CUSTO_SERVIDOR_CLOUD: custo_servidor_cloud
                };

                if(obj.QTD_TRADES <= 0) {
                    obj.resultado = -1000000000;
                }

                resultado_geracao.splice(j, 0, obj);
                break;
            }
    
            if(data_atual >= data_inicio_analise) {
                qtd_bars ++;
                
                data_atual_debug = serie_dados[i].data_obj.data_completa;
                hora_atual_debug = serie_dados[i].data_obj.hora_completa;
                dia_da_semana = data_atual.getDay();
                dia_mes_do_ano = data_atual.getMonth();

                //console.log(`price_low[3]: ${price_low[1]} - price_high[3]: ${price_high[3]}`);

                if(
                    price_high[3] >= price_high[1] &&
                    price_high[3] >= price_high[2] &&
                    price_high[3] >= price_high[4] &&
                    price_high[3] >= price_high[5] &&
                    price_high[3] >= price_high[6]  
                ) {
                    maximo_atual = price_high[3];
                    //console.log(`maximo_atual: ${maximo_atual}`);
                }

                if(
                    price_low[3] <= price_low[1] &&
                    price_low[3] <= price_low[2] &&
                    price_low[3] <= price_low[4] &&
                    price_low[3] <= price_low[5] &&
                    price_low[3] <= price_low[6]  
                ) {
                    minimo_atual = price_low[3];
                    //console.log(`minimo_atual: ${minimo_atual}`);
                }

                dormiu_posicionado(qtd_bars, price_open[0], `${data_atual_debug}:${hora_atual_debug}` );
    
                hora_maxima_negociacao(qtd_bars, price_open[0]);
    
                reset_valores_dia(serie_dados[i]);
    
                if(!COMPRADO && !VENDIDO) {
                    await se_posicionar(individuo);
                } else {
                    await zerar_posicao();
                }
            }
        }

        resolve("terminou day trade");
    });

    return await individuo_day_trade;
}

const run_populacao_day_trade = async (populacao, serie_dados, data_inicio_analise, data_fim_analise) => {
    const new_populacao_day_trade = new Promise(async (resolve, reject) => {
        
        for(j=0; j<populacao.length; j++) {
            await run_day_trade(populacao[j], serie_dados, data_inicio_analise, data_fim_analise);
            reset_variaveis_globais();
            console.log(`run_day_trade: ${j}`);
            text_log += `run_day_trade: ${j}\n`;
        }
    
        resolve("terminou new populacao day trade");
    });

    return await new_populacao_day_trade;
}

const initialize = () => {
    reset_variaveis_globais();

    price_open = [];
    price_high = [];
    price_low = [];
    price_close = [];

    price_open.reverse();
    price_high.reverse();
    price_low.reverse();
    price_close.reverse();
    qtd_bars = 0;

    populacao = [];
    neuronios = [];
    qtd_geracao = 0;
}

const get_data = (dados) => {
    price_open.unshift(dados.open);
    if(price_open.length >= periodo_candles+1) {
        price_open.pop();
    }

    price_high.unshift(dados.high);
    if(price_high.length >= periodo_candles+1) {
        price_high.pop();
    }

    price_low.unshift(dados.low);
    if(price_low.length >= periodo_candles+1) {
        price_low.pop();
    }

    price_close.unshift(dados.close);
    if(price_close.length >= periodo_candles+1) {
        price_close.pop();
    }
}

const reset_valores_dia = (dados) => {
    if(dia_atual != dados.data_obj.dia) {
        // console.log('qtd_bars: ',qtd_bars);
        // console.log(price_open);
        // console.log(price_high);
        // console.log(price_low);
        // console.log(price_close);
        // console.log(dados);
        preco_abertura = price_open[0];
        preco_max = preco_abertura + 6000;
        preco_min = preco_abertura - 6000;
        maximo_atual = 0;
        minimo_atual = 0;
        qtd_bars = 0;
        dia_atual = dados.data_obj.dia;
        // console.log(`data: ${dados.data_obj.data_completa} / BALANCE: ${BALANCE}`);
    }
}

const se_posicionar_comprado = async (price, stop_loss, stop_gain) => {
    const set_posicao_comprado = new Promise((resolve, reject) => {
        POSICAO = price;
        STOP_LOSS = stop_loss;
        STOP_GAIN = stop_gain;
        COMPRADO = true;
        QTD_TRADES ++;

        // console.log(`(comprado) - price: ${price} - stop_loss: ${stop_loss} - stop_gain: ${stop_gain} - qtd_bars: ${qtd_bars}`);
        resolve('comprado');
    });

    return await set_posicao_comprado;
    
}

const se_posicionar_vendido = async (price, stop_loss, stop_gain) => {
    const set_posicao_vendido = new Promise((resolve, reject) => {
        POSICAO = price;
        STOP_LOSS = stop_loss;
        STOP_GAIN = stop_gain;
        VENDIDO = true;
        QTD_TRADES ++;

        // console.log(`(vendido) - price: ${price} - stop_loss: ${stop_loss} - stop_gain: ${stop_gain} - qtd_bars: ${qtd_bars}`);
        resolve('vendido');
    });

    return await set_posicao_vendido;
}

const se_posicionar = async (individuo) => {
    
    if(qtd_bars <= min_qtd_bars) {
        return true;
    }

    if(qtd_bars >= max_qtd_bars_posicao) {
        return true;
    }

    for(let i=0, count = 1; i < neuronios[0].length - 1 - qtd_neuronio_entrada_adicional; i++) { 
        if(count >= periodo_candles - 1) {
            count = 1;
        }

        neuronios[0][i] = preco_max - price_close[count];
        i++;
        
        neuronios[0][i] = price_close[count] - preco_min;
        i++;

        neuronios[0][i] = preco_max - price_open[count];
        i++;
        
        neuronios[0][i] = price_open[count] - preco_min;
        i++;

        //let distancia_entre_candle = (price_close[1] - price_close[count])*(price_close[1] - price_close[count]) + (count-1)*(count-1);
        //neuronios[0][i] = parseFloat((Math.pow( distancia_entre_candle , 0.5)).toFixed(2));
        //i++;

        neuronios[0][i] = 108 - qtd_bars - count;
        
        count ++;
    }
    
    let index_neuronio_entrada = neuronios[0].length - qtd_neuronio_entrada_adicional - 1;
    //neuronios[0][index_neuronio_entrada] = qtd_bars;

    //index_neuronio_entrada ++;

    neuronios[0][index_neuronio_entrada] = (dia_da_semana + 1);

    //console.log('neuronios[0]', neuronios[0]);

    for(let l=0; l < neuronios[1].length; l++ ) {
        neuronios[1][l] = await reLU(neuronios[0], individuo[0][l]);
    }
    
    //console.log('neuronios[1]', neuronios[1]);

    for(let k=0; k < neuronios[2].length; k++ ) {
        neuronios[2][k] = await reLU(neuronios[1], individuo[1][k]);
    }

    //console.log('neuronios[2]', neuronios[2]);

    for(let m=0; m < neuronios[3].length; m++ ) {
        neuronios[3][m] = await reLU(neuronios[2], individuo[2][m]);
    }

    //console.log('neuronios[3]', neuronios[3]);

    await escolher_saida(neuronios[3]);
    
}

const reset_posicao_comprado = () => {
    POSICAO = null;
    STOP_LOSS = null;
    STOP_GAIN = null;
    COMPRADO = false;
}

const reset_posicao_vendido = () => {
    POSICAO = null;
    STOP_LOSS = null;
    STOP_GAIN = null;
    VENDIDO = false;
}

const close_posicao = (price_close, data) => {

    if(COMPRADO && VENDIDO) {
        console.log(`Erro :${data} está comprado e vendido ao mesmo tempo`);
    }

    if(COMPRADO) {
        let profit = ((price_close - POSICAO)/5) - desconto_por_trade;

        if(profit <= 0) {
            QTD_ERROS ++;
            QTD_ERROS_VENDIDO ++;
            PERDA_BRUTA += profit;
            profit -= penalidade_por_erro_trade;
            PENALIDADE_ERRO_TRADE -= penalidade_por_erro_trade;
        }

        if(profit  > 0) {
            QTD_ACERTOS ++;
            QTD_ACERTOS_VENDIDO ++;
            LUCRO_BRUTO += profit;
        }

        BALANCE += profit - custo_por_trader;
        reset_posicao_comprado();
    }
    
    if(VENDIDO) {
        let profit = (price_close - POSICAO)*(-1)/5 - desconto_por_trade;

        if(profit <= 0) {
            QTD_ERROS ++;
            QTD_ERROS_COMPRADO ++;
            PERDA_BRUTA += profit;
            profit -= penalidade_por_erro_trade;
            PENALIDADE_ERRO_TRADE -= penalidade_por_erro_trade;
        }

        if(profit  > 0) {
            QTD_ACERTOS ++;
            QTD_ACERTOS_COMPRADO ++;
            LUCRO_BRUTO += profit;
        }

        BALANCE += profit - custo_por_trader;
        reset_posicao_vendido();
    }
}

const zerar_posicao = () => {
    if(COMPRADO) {
        if(
            COMPRADO &&
            price_high[1] >= STOP_GAIN
            //price_close[1] >= STOP_GAIN
        ) {
            close_posicao(price_open[0]);
            //STOP_LOSS = price_low[1];
            //STOP_GAIN = price_high[1] + (price_high[1] - price_low[1])*1.5;
        }

        if(
            COMPRADO &&
            price_close[1] <= STOP_LOSS
        ) {
            close_posicao(price_open[0]);
        }
    }

    if(VENDIDO) {
        if(
            VENDIDO &&
            price_low[1] <= STOP_GAIN
            //price_close[1] <= STOP_GAIN
        ) {
            close_posicao(price_open[0]);
            //STOP_LOSS = price_high[1];
            //STOP_GAIN = price_low[1] - (price_high[1] - price_low[1])*1.5;
        }

        if(
            VENDIDO &&
            price_close[1] >= STOP_LOSS
        ) {
            close_posicao(price_open[0]);
        }
    }
}

const hora_maxima_negociacao = (qtd_bar, price) => {
    if(qtd_bar >= max_qtd_bars_posicao && (COMPRADO || VENDIDO)) {
        close_posicao(price);
    }
}

const dormiu_posicionado = (qtd_bar, price, data) => {
    if(qtd_bar <= min_qtd_bars && (COMPRADO || VENDIDO)) {
        console.log(`Erro : ${data} dormiu posicionado`);
        close_posicao(price);
    }
}

const reset_variaveis_globais = () => {
    if(COMPRADO) {
        //console.log(`Erro : reset_variaveis_globais comprado`);
        text_log += `Erro : reset_variaveis_globais comprado\n`;
    }

    if(VENDIDO) {
        //console.log(`Erro : reset_variaveis_globais vendido`);
        text_log += `Erro : reset_variaveis_globais vendido\n`;
    }

    COMPRADO = false;
    VENDIDO = false;
    POSICAO = null;
    STOP_LOSS = null;
    STOP_GAIN = null;
    BALANCE = 0;
    QTD_ERROS = 0;
    QTD_ACERTOS = 0;
    LUCRO_BRUTO = 0;
    PERDA_BRUTA = 0;
    QTD_TRADES = 0;
    QTD_ACERTOS_COMPRADO = 0;
    QTD_ERROS_COMPRADO = 0;
    QTD_ACERTOS_VENDIDO = 0;
    QTD_ERROS_VENDIDO = 0;
    PENALIDADE_ERRO_TRADE = 0;
}

const build_rede_gen_0 = async () => {
    let rede_neural = [];
    let rede_entrada = [];
    let camada_1 = [];
    let camada_2 = [];
    
    const rede = new Promise(async (resolve, reject) => {
        for(let i = 0; i <= qtd_n_rede_camada_escondida_1; i++) {
            let linha = [];
            for(let j = 0; j <= qtd_n_entradas; j++) {
                linha[j] = parseInt(((Math.random())*(Math.random() >= 0.5 ? valor_maximo_pesos : -valor_maximo_pesos)));
            }
    
            rede_entrada.push(linha); 
        }
    
        rede_neural.push(rede_entrada);
    
        for(let i = 0; i <= qtd_n_rede_camada_escondida_2; i++) {
            let linha = [];
            for(let j = 0; j <= qtd_n_rede_camada_escondida_1; j++) {
                linha[j] = parseInt(((Math.random())*(Math.random() >= 0.5 ? valor_maximo_pesos : -valor_maximo_pesos)));
            }
    
            camada_1.push(linha); 
        }
        
        rede_neural.push(camada_1);
    
        for(let i = 0; i <= redes_saidas; i++) {
            let linha = [];
            for(let j = 0; j <= qtd_n_rede_camada_escondida_2; j++) {
                linha[j] = parseInt(((Math.random())*(Math.random() >= 0.5 ? valor_maximo_pesos : -valor_maximo_pesos)));
            }
    
            camada_2.push(linha); 
        }
        
        rede_neural.push(camada_2);

        resolve(rede_neural);
    });
    
    
    return await rede;
}

const build_rede_gen_x = async (melhor_rede) => {
    let array_ordem_camadas_escondidas = [];
    let rede_neural = [];
    let rede_entrada = [];
    let camada_1 = [];
    let camada_2 = [];

    array_ordem_camadas_escondidas.push(qtd_n_rede_camada_escondida_1);
    array_ordem_camadas_escondidas.push(qtd_n_rede_camada_escondida_2);

    const rede = new Promise(async (resolve, reject) => {
        const nivel_mutacao = valor_maximo_pesos*variacao_pesos_gen_x;

        for(let i = 0; i <= qtd_n_rede_camada_escondida_1; i++) {
            let linha = [];
            for(let j = 0; j <= qtd_n_entradas; j++) {
                linha[j] = melhor_rede[0][i][j] + (parseInt( ( (Math.random() )*(Math.random() >= 0.5 ? nivel_mutacao : -nivel_mutacao)))*(Math.random() >= porcentagem_caracteristica_melhor ? 1 : 0));

                if(limitar_valor_maximo_pesos) {
                    if(linha[j] <= -valor_maximo_pesos) {
                        linha[j] = -valor_maximo_pesos;
                    }
    
                    if(linha[j] >= valor_maximo_pesos) {
                        linha[j] = valor_maximo_pesos;
                    }
                }
                
            }
    
            rede_entrada.push(linha); 
        }
    
        rede_neural.push(rede_entrada);
    
        for(let i = 0; i <= qtd_n_rede_camada_escondida_2; i++) {
            let linha = [];
            for(let j = 0; j <= qtd_n_rede_camada_escondida_1; j++) {
                linha[j] = melhor_rede[1][i][j] + (parseInt( ( (Math.random() )*(Math.random() >= 0.5 ? nivel_mutacao : -nivel_mutacao)))*(Math.random() >= porcentagem_caracteristica_melhor ? 1 : 0));

                if(limitar_valor_maximo_pesos) {
                    if(linha[j] <= -valor_maximo_pesos) {
                        linha[j] = -valor_maximo_pesos;
                    }
    
                    if(linha[j] >= valor_maximo_pesos) {
                        linha[j] = valor_maximo_pesos;
                    }
                }
            }
    
            camada_1.push(linha); 
        }
        
        rede_neural.push(camada_1);
    
        for(let i = 0; i <= redes_saidas; i++) {
            let linha = [];
            for(let j = 0; j <= qtd_n_rede_camada_escondida_2; j++) {
                linha[j] = melhor_rede[2][i][j] + (parseInt( ( (Math.random() )*(Math.random() >= 0.5 ? nivel_mutacao : -nivel_mutacao)))*(Math.random() >= porcentagem_caracteristica_melhor ? 1 : 0));

                if(limitar_valor_maximo_pesos) {
                    if(linha[j] <= -valor_maximo_pesos) {
                        linha[j] = -valor_maximo_pesos;
                    }
    
                    if(linha[j] >= valor_maximo_pesos) {
                        linha[j] = valor_maximo_pesos;
                    }
                }
            }
    
            camada_2.push(linha); 
        }
        
        rede_neural.push(camada_2);

        resolve(rede_neural);
    });
    
    
    return await rede;
}

const build_populacao = async (melhor_individuo_gen) => {
    const new_populacao_gerada = new Promise(async (resolve, reject) => {
        let new_populacao = [];
        
        if(qtd_geracao <= 0 && melhor_individuo_gen.length <= 0) {
            //console.log("build_rede_gen_0");
            text_log += `build_rede_gen_0\n`;
            for(let i = 0; i < qtd_populacao; i++) {
                const new_individuo = await build_rede_gen_0();
                new_populacao.push(new_individuo);
            }

            // resolve(new_populacao);
            // return;
        }

        if(
            qtd_geracao > 0 && melhor_individuo_gen.length <= 0
        ) {
            //console.log("build_rede_gen_x SEM o melhor da epoca");
            text_log += `build_rede_gen_x SEM o melhor da epoca\n`;
            const melhor_individuo = await get_melhor_individuo();
            new_populacao.push(melhor_individuo);

            for(let i = 1; i < qtd_populacao; i++) {
                const new_individuo = await build_rede_gen_x(melhor_individuo);
                new_populacao.push(new_individuo);
            }

            // resolve(new_populacao);
            // return;
        }

        if(
            qtd_geracao == 1 && melhor_individuo_gen.length > 0
        ) {
            //console.log("build_rede_gen_x COM o melhor da epoca");
            text_log += `build_rede_gen_x COM o melhor da epoca\n`;
            new_populacao.push(melhor_individuo_gen);

            for(let i = 1; i < qtd_populacao; i++) {
                const new_individuo = await build_rede_gen_x(melhor_individuo_gen);
                new_populacao.push(new_individuo);
            }

            // resolve(new_populacao);
            // return;
        }

        if(
            qtd_geracao > 1 && melhor_individuo_gen.length > 0
        ) {
            //console.log("build_rede_gen_x SEM o melhor da epoca (apesar de existir ele)");
            text_log += `build_rede_gen_x SEM o melhor da epoca (apesar de existir ele)\n`;
            const melhor_individuo = await get_melhor_individuo();
            new_populacao.push(melhor_individuo);

            for(let i = 1; i < qtd_populacao; i++) {
                const new_individuo = await build_rede_gen_x(melhor_individuo);
                new_populacao.push(new_individuo);
            }

            // resolve(new_populacao);
            // return;
        }
        
        resolve(new_populacao);
    });
    
    return await new_populacao_gerada;
}

const get_melhor_individuo = async () => {
    const index = await index_melhor(resultado_geracao);
    return populacao[index];
}

const index_melhor = async (result_geracao) => {
    let index = 0;
    let lucro = result_geracao[0].resultado;

    let index_melhor = new Promise ((resolve, reject) => {
        
        for(let i=0; i < result_geracao.length; i++) {

            if(result_geracao[i].resultado >= lucro) {
                lucro = result_geracao[i].resultado;
                index = i;
            }
        }

        //===================Resultado Melhor Individuo==================
        // console.log(result_geracao[index]);
        if(result_geracao[index].resultado > 0) {
            const resultado = {
                pesos: populacao[index],
                result: result_geracao[index]
                
            };

            melhores_da_resultados.push(resultado);
        }

        if(
            result_geracao[index].resultado <= 0 && 
            qtd_geracao >= qtd_max_geracao && 
            melhores_da_resultados.length <=0
        ) {
            const resultado = {
                pesos: populacao[index],
                result: result_geracao[index]
                
            };

            melhores_da_resultados.push(resultado);
        }

        
        //===============================================================

        resolve(index);
    });

    return await index_melhor;
}

/*const index_melhor = async (result_geracao) => {
    let index = 0;
    let fator_lucro = result_geracao[0].FATOR_LUCRO;

    let index_melhor = new Promise ((resolve, reject) => {
        
        for(let i=0; i < result_geracao.length; i++) {

            if(result_geracao[i].FATOR_LUCRO >= fator_lucro) {
                fator_lucro = result_geracao[i].FATOR_LUCRO;
                index = i;
            }
        }

        //===================Resultado Melhor Individuo==================
        // console.log(result_geracao[index]);
        if(result_geracao[index].FATOR_LUCRO > 0) {
            const resultado = {
                pesos: populacao[index],
                result: result_geracao[index]
                
            };

            melhores_da_resultados.push(resultado);
        }

        if(
            result_geracao[index].FATOR_LUCRO <= 0 && 
            qtd_geracao >= qtd_max_geracao && 
            melhores_da_resultados.length <=0
        ) {
            const resultado = {
                pesos: populacao[index],
                result: result_geracao[index]
            };

            melhores_da_resultados.push(resultado);
        }

        
        //===============================================================

        resolve(index);
    });

    return await index_melhor;
}*/

/*const index_melhor = async (result_geracao) => {
    let index = 0;
    let acertos = result_geracao[0].ACERTOS;

    let index_melhor = new Promise ((resolve, reject) => {
        
        for(let i=0; i < result_geracao.length; i++) {

            if(result_geracao[i].ACERTOS >= acertos) {
                acertos = result_geracao[i].ACERTOS;
                index = i;
            }
        }

        //===================Resultado Melhor Individuo==================
        // console.log(result_geracao[index]);
        if(result_geracao[index].ACERTOS > 0) {
            const resultado = {
                pesos: populacao[index],
                result: result_geracao[index]
                
            };

            melhores_da_resultados.push(resultado);
        }

        if(
            result_geracao[index].ACERTOS <= 0 && 
            qtd_geracao >= qtd_max_geracao && 
            melhores_da_resultados.length <=0
        ) {
            const resultado = {
                pesos: populacao[index],
                result: result_geracao[index]
            };

            melhores_da_resultados.push(resultado);
        }

        
        //===============================================================

        resolve(index);
    });

    return await index_melhor;
}*/

const get_id_saida_mais_ativada = async(saidas) => {
    const res_id = new Promise((resolve, reject) => {
        let index_saida = 0;
        let maior_valor = saidas[0];

        for(let i = 0; i < saidas.length; i++) {
            if(maior_valor <= saidas[i]) {
                index_saida = i;
                maior_valor = saidas[i];
            }
        }

        resolve(index_saida);
    });

    return await res_id;
}

const escolher_saida = async(saidas) => {

    const id_saida = await get_id_saida_mais_ativada(saidas);
    // console.log(`id: ${id_saida}`);
    
    switch(id_saida) {
    
        case 0: {
            se_posicionar_comprado(price_close[1], price_low[2] - (price_high[1] - price_low[1]) , price_close[1] + ( (price_close[1] - price_low[2]) + (price_high[1] - price_low[1])) *0.5 ); 
        } break;

        case 1: {
            se_posicionar_comprado(price_close[1], price_low[2] - (price_high[1] - price_low[1]) , price_close[1] + ( (price_close[1] - price_low[2]) + (price_high[1] - price_low[1])) *1 ); 
        } break;

        case 2: {
            se_posicionar_comprado(price_close[1], price_low[2] - (price_high[1] - price_low[1]), price_close[1] + ( (price_close[1] - price_low[2]) + (price_high[1] - price_low[1])) * 1.5); 
        } break;

        case 3: {
            se_posicionar_comprado(price_close[1], price_low[3] - (price_high[1] - price_low[1]), price_close[1] + ( (price_close[1] - price_low[3]) + (price_high[1] - price_low[1])) * 0.5); 
        } break;

        case 4: {
            se_posicionar_comprado(price_close[1], price_low[3] + (price_high[1] - price_low[1]), price_close[1] + ( (price_close[1] - price_low[3]) + (price_high[1] - price_low[1])) *1 );
        } break;

        case 5: {
            se_posicionar_comprado(price_close[1], price_low[3] + (price_high[1] - price_low[1]), price_close[1] + ( (price_close[1] - price_low[3]) + (price_high[1] - price_low[1])) * 1.5);
        } break;

        case 6: {
            se_posicionar_vendido(price_close[1], price_high[2] + (price_high[1] - price_low[1]), price_close[1] - ( price_high[2] - price_close[1] + (price_high[1] - price_low[1] ) ) * 0.5);
        } break;

        case 7: {
            se_posicionar_vendido(price_close[1], price_high[2] + (price_high[1] - price_low[1]), price_close[1] - ( price_high[2] - price_close[1] + (price_high[1] - price_low[1] ) ) * 1 );
        } break;

        case 8: {
            se_posicionar_vendido(price_close[1], price_high[2] + (price_high[1] - price_low[1]), price_close[1] - (price_high[1] - price_low[1])*1.5);
        } break;

        case 9: {
            se_posicionar_vendido(price_close[1], price_high[3] + (price_high[1] - price_low[1]), price_close[1] - ( price_high[3] - price_close[1] + (price_high[1] - price_low[1] ) ) * 0.5);
        } break;

        case 10: {
            se_posicionar_vendido(price_close[1], price_high[3] + (price_high[1] - price_low[1]), price_close[1] - ( price_high[3] - price_close[1] + (price_high[1] - price_low[1] ) ) * 1);
        } break;

        case 11: {
            se_posicionar_vendido(price_close[1], price_high[3] + (price_high[1] - price_low[1]), price_close[1] - ( price_high[3] - price_close[1] + (price_high[1] - price_low[1] ) ) * 1.5);
        } break;

        case 12: {
            //console.log("Calma, observe mais !!!!!")
        } break;
    }
}

const organiza_geracao = async(melhores_geracoes) => {
    const melhores = await seleciona_melhores(melhores_geracoes);
    return await ordena_melhores(melhores);
}

const seleciona_melhores = async(melhores) => {
    const melhores_selecionados = new Promise((resolve, reject) => {
        let melhore_individuos = [];

        for(let i=0; i < melhores.length; i++) {
            let existe_no_melhore_individuos = false;

            for(let k=0; k < melhore_individuos.length; k++) {
                if(
                    melhore_individuos[k].result.resultado == melhores[i].result.resultado &&
                    melhore_individuos[k].result.QTD_TRADES == melhores[i].result.QTD_TRADES &&
                    melhore_individuos[k].result.QTD_ACERTOS == melhores[i].result.QTD_ACERTOS &&
                    melhore_individuos[k].result.QTD_ERROS == melhores[i].result.QTD_ERROS
                    
                ) {
                    existe_no_melhore_individuos = true;
                    break;
                }
            }

            if(!existe_no_melhore_individuos) {
                melhore_individuos.push(melhores[i]);
            }
        }

        resolve(melhore_individuos);
    });

    return await melhores_selecionados;
}

const ordena_melhores = async(array_melhores_selecionado) => {
    const ordenar_melhor = new Promise((resolve, reject)=>{
        array_melhores_selecionado.sort((a, b) => {
            return b.result.resultado - a.result.resultado;
        });
        
        resolve(array_melhores_selecionado);
    });

    return await ordenar_melhor;
}

const registra_log_rede = async (text) => {
    const write_file = new Promise((resolve, reject) => {
        const path_file = `${__dirname}/rede_log.txt`;

        fs.appendFile(path_file, text, function (err, file) {
            if (err) {
                console.log(err);
                throw  err;
            } 
            resolve('Saved!');
        });
    });
    
    return await write_file;
}

const rede_ja_gravada = async(resultado, qtd_trades) => {
    for(let i=0; i < redes_gravadas.length; i++) {

        if(redes_gravadas[i].result.resultado == resultado && redes_gravadas[i].result.QTD_TRADES == qtd_trades) {
            return true;
        }
    }

    return false;
}

const run_script_rede_neural = async () => {
    console.log("Iniciou.........");
    qtd_epoca = 0;
    let geracao_geral = await start(melhor_individuo_inicial);
    let geracao = await organiza_geracao(geracao_geral);
    
    console.log(`epoca zero: ${0}`);
    text_log += `epoca zero: ${0}\n`;

    if( qtd_epoca >= qtd_max_epoca*0.6 && variacao_pesos_gen_x != 0.9 && porcentagem_caracteristica_melhor != 0.1) {
        variacao_pesos_gen_x = 0.9;
        porcentagem_caracteristica_melhor = 0.1; 
    }
    
    await registra_log_rede(text_log);
    text_log = "";

    for(let qtd_epoca = 0; qtd_epoca <= qtd_max_epoca; qtd_epoca++) {
            qtd_geracao = 1;
            geracao_geral = await start(geracao[0].pesos);
            geracao = await organiza_geracao(geracao_geral);

            console.log(`epoca for: ${qtd_epoca}`);
            text_log += `epoca for: ${qtd_epoca}\n`;

            for(let qtd_gen = 0; qtd_gen < geracao.length; qtd_gen++) {
                if(geracao.length <= 1 && qtd_gen > 1) {
                    break;
                }
        
                if(qtd_gen >= 2) {
                    break;
                }
                
                //if(geracao[qtd_gen].result.resultado > 0) {
                    const ja_gravado = await rede_ja_gravada(geracao[qtd_gen].result.resultado, geracao[qtd_gen].result.QTD_TRADES);

                    if(!ja_gravado) {
                        redes_gravadas.push(geracao[qtd_gen]);
                        text_log += `\n\n=================================================\n${JSON.stringify(geracao[qtd_gen])}\n=======================================================\n\n`;
                    }
               // }
            }
            
            await registra_log_rede(text_log);
            text_log = "";
    }

    console.log("Terminou...........");
    const result = {
        qtd_geracao: qtd_max_geracao,
        data: `${data_inicial_treino} ate ${data_final_treino}`,
        geracao
    };

    text_log = null;

    text_log += `\n\n=================================================\n${JSON.stringify(result)}\n=======================================================\n\n`;
    await registra_log_rede(text_log);
}

run_script_rede_neural();

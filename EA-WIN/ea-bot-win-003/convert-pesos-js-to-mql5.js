var fs = require('fs');
var melhores_redes = require('./melhores-redes');
const { parametros_metatrader, parametros_rede_neural} = require('./variaveis-globais');

var melhor_individuo_inicial = melhores_redes.geracao_1[0].pesos;

const qtd_n_entradas = parametros_rede_neural.qtd_n_entradas;
const qtd_n_rede_camada_escondida_1 = parametros_rede_neural.qtd_n_rede_camada_escondida_1;
const qtd_n_rede_camada_escondida_2 = parametros_rede_neural.qtd_n_rede_camada_escondida_2;
const redes_saidas = parametros_rede_neural.redes_saidas;

const convert_pesos_js_to_mql5 = async () => {
    neuronios = [];

    let text = "====================================== entrada ("+melhor_individuo_inicial[0][0].length+")===================================\n"; 
    text +=  JSON.stringify(melhor_individuo_inicial[0]).replace(/\[/g, "{").replace(/\]/g, "}");
    text += "\n==================================================================================\n\n\n";

    text += "====================================== camada 1 ("+melhor_individuo_inicial[1][0].length+")===================================\n"; 
    text +=  JSON.stringify(melhor_individuo_inicial[1]).replace(/\[/g, "{").replace(/\]/g, "}");
    text += "\n==================================================================================\n\n\n";

    text += "====================================== camada 2 ("+melhor_individuo_inicial[2][0].length+")===================================\n"; 
    text +=  JSON.stringify(melhor_individuo_inicial[2]).replace(/\[/g, "{").replace(/\]/g, "}");
    text += "\n==================================================================================\n\n\n";

    neuronios = await build_neuronios();

    text += "====================================== neuronio entrada ("+neuronios[0].length+")===================================\n"; 
    text +=  JSON.stringify(neuronios[0]).replace(/\[/g, "{").replace(/\]/g, "}");
    text += "\n==================================================================================\n\n\n";

    text += "====================================== neuronio camada 1 ("+neuronios[1].length+")===================================\n"; 
    text +=  JSON.stringify(neuronios[1]).replace(/\[/g, "{").replace(/\]/g, "}");
    text += "\n==================================================================================\n\n\n";

    text += "====================================== neuronio camada 2 ("+neuronios[2].length+")===================================\n"; 
    text +=  JSON.stringify(neuronios[2]).replace(/\[/g, "{").replace(/\]/g, "}");
    text += "\n==================================================================================\n\n\n";

    text += "====================================== neuronio saida ("+neuronios[3].length+")===================================\n"; 
    text +=  JSON.stringify(neuronios[3]).replace(/\[/g, "{").replace(/\]/g, "}");
    text += "\n==================================================================================\n\n\n";

    await salva_pesos_convertidos(text);
}

const salva_pesos_convertidos = async (text) => {
    const write_file = new Promise((resolve, reject) => {
        const path_file = `${__dirname}/mql5.txt`;

        fs.writeFile(path_file, text, function (err, file) {
            if (err) {
                console.log(err);
                throw  err;
            } 
            resolve('Saved!');
        });
    });
    
    return await write_file;
}

const build_neuronios = async () => {
    let neuronios = [];
    let camada_1 = [];
    let camada_2 = [];

    const rede = new Promise(async (resolve, reject) => {

        let camada_entrada = [];
        for(let i = 0; i < qtd_n_entradas; i++) {
            camada_entrada[i] = 0;
        }

        camada_entrada[qtd_n_entradas] = 1;

        neuronios.push(camada_entrada); 
        
        for(let i = 0; i < qtd_n_rede_camada_escondida_1; i++) {
            camada_1[i] = 0; 
        }
        
        camada_1[qtd_n_rede_camada_escondida_1] = 1;

        neuronios.push(camada_1);
    
        for(let i = 0; i <= qtd_n_rede_camada_escondida_2; i++) {
            camada_2[i] = 0; 
        }
        
        camada_2[qtd_n_rede_camada_escondida_2] = 1;

        neuronios.push(camada_2);

        let camada_saida = [];
        for(let i = 0; i < redes_saidas; i++) {
            camada_saida[i] = 0;
        }

        neuronios.push(camada_saida);

        resolve(neuronios);
    });
    
    
    return await rede;
}

convert_pesos_js_to_mql5();
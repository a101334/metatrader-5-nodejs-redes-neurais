var http = require('http');
var melhores_redes = require('./melhores-redes');
const { parametros_metatrader, parametros_rede_neural} = require('./variaveis-globais');
const { build_neuronios, reLU, create_data } = require('./funcoes-redes');

var melhor_individuo_inicial = [];
// var serie_dados = [];

var data_inicial_treino = parametros_metatrader.data_inicial_treino;
var data_final_treino = parametros_metatrader.data_final_treino;
var custo_por_trader = parametros_metatrader.custo_por_trader;
var desconto_por_trade = parametros_metatrader.desconto_por_trade;

//==========Geracao
var qtd_max_geracao = 5;
var melhores_da_resultados = [];
//==================

var periodo_candles = parametros_metatrader.periodo_candles;
var qtd_neuronio_entrada_adicional = parametros_metatrader.qtd_neuronio_entrada_adicional;
var min_qtd_bars = parametros_metatrader.min_qtd_bars;
var max_qtd_bars_posicao = parametros_metatrader.max_qtd_bars_posicao;

var qtd_bars = 0;
var dia_atual = "";
var dia_da_semana = "";
var dia_mes_do_ano = "";
var preco_abertura = 0;
var data_atual_debug = "";
var hora_atual_debug = "";
var modo_debug_posicao = false;
var modo_debug_profit = false;
var modo_debug_saida = false;
var modo_debug_neuronios = false;

var price_open = [];
var price_high = [];
var price_low = [];
var price_close = [];

//Redes neurais
const qtd_n_entradas = parametros_rede_neural.qtd_n_entradas;
const qtd_n_rede_camada_escondida_1 = parametros_rede_neural.qtd_n_rede_camada_escondida_1;
const qtd_n_rede_camada_escondida_2 = parametros_rede_neural.qtd_n_rede_camada_escondida_2;
const redes_saidas = parametros_rede_neural.redes_saidas;
//========================

var i=0;
var j=0;

//Variaveis globais metatrader
var COMPRADO = false;
var VENDIDO = false;
var POSICAO = null;
var STOP_LOSS = null;
var STOP_GAIN = null;
var BALANCE = 0;
var QTD_ERROS = 0;
var QTD_ACERTOS = 0;
var LUCRO_BRUTO = 0;
var PERDA_BRUTA = 0;
var QTD_TRADES = 0;
var QTD_ACERTOS_COMPRADO = 0;
var QTD_ERROS_COMPRADO = 0;
var QTD_ACERTOS_VENDIDO = 0;
var QTD_ERROS_VENDIDO = 0;

var text_log = "";

const start = async (melhor_individuo_geracao) => {
    
    initialize();
    
    // `../../series-historicas/WIN/WINNM5-treino-min.csv`
    // `../../series-historicas/WIN/WINNM5-treino.csv`
    // `../../series-historicas/WIN/WINNM5-treino-v3.csv`
    //  `../../series-historicas/WIN/WINNM5.csv`
    //  `../../series-historicas/WIN/WINNM5-validacao.csv`
    //  `../../series-historicas/WIN/WINNM5-validacao2.csv`

    const serie_dados = await create_data(`../../series-historicas/WIN/WINNM5-treino.csv`);
    let data_inicio_analise = new Date(data_inicial_treino);
    let data_fim_analise = new Date(data_final_treino);

    melhores_da_resultados = [];

    neuronios = [];
    populacao = [];
    populacao.push(melhores_redes.geracao_1[0].pesos);

    neuronios = await build_neuronios(qtd_n_entradas, qtd_n_rede_camada_escondida_1, qtd_n_rede_camada_escondida_2, redes_saidas);
    await run_populacao_day_trade(populacao, serie_dados, data_inicio_analise, data_fim_analise);

    i=0;
    j=0;

    return melhores_da_resultados;
}

const run_day_trade = async (individuo, serie_dados, data_inicio_analise, data_fim_analise) => {
    const individuo_day_trade = new Promise( async (resolve, reject) => {
        for(let i=1; i < serie_dados.length; i++) {
            get_data(serie_dados[i]);
            
            if(!serie_dados[i].hasOwnProperty("data_obj")) {
                continue;
            }
            
            let data_atual = new Date(serie_dados[i].data_obj.data_completa);
            
            if(data_atual >= data_fim_analise || i >= serie_dados.length - 2) {
                let perda_bruta_fator_lucro = PERDA_BRUTA;
                if(perda_bruta_fator_lucro == 0) {
                    perda_bruta_fator_lucro = -0.1;
                }

                let obj = {
                    resultado: BALANCE,
                    QTD_TRADES: QTD_TRADES,
                    QTD_ACERTOS: QTD_ACERTOS,
                    QTD_ERROS: QTD_ERROS,
                    ACERTOS: ((QTD_ACERTOS/QTD_TRADES)*100),
                    ERROS: ((QTD_ERROS/QTD_TRADES)*100),
                    LUCRO_LIQUIDO: (LUCRO_BRUTO + PERDA_BRUTA),
                    LUCRO_BRUTO: LUCRO_BRUTO,
                    PERDA_BRUTA: PERDA_BRUTA,
                    FATOR_LUCRO: (LUCRO_BRUTO/(perda_bruta_fator_lucro*(-1))),
                    QTD_ACERTOS_COMPRADO: QTD_ACERTOS_COMPRADO,
                    QTD_ERROS_COMPRADO: QTD_ERROS_COMPRADO,
                    ACERTOS_COMRPADO: (QTD_ACERTOS_COMPRADO/(QTD_ACERTOS_COMPRADO + QTD_ERROS_COMPRADO)),
                    QTD_ACERTOS_VENDIDO: QTD_ACERTOS_VENDIDO,
                    QTD_ERROS_VENDIDO: QTD_ERROS_VENDIDO,
                    ACERTOS_VENDIDO: (QTD_ACERTOS_VENDIDO/(QTD_ACERTOS_VENDIDO + QTD_ERROS_VENDIDO))
                };

                if(obj.QTD_TRADES <= 0) {
                    obj.resultado = -1000000000;
                }

                const resultado = {
                    pesos: individuo,
                    result: obj
                };
                
                melhores_da_resultados.push(resultado);
                reset_variaveis_globais();
                break;
            }
    
            if(data_atual >= data_inicio_analise) {
                qtd_bars ++;

                data_atual_debug = serie_dados[i].data_obj.data_completa;
                hora_atual_debug = serie_dados[i].data_obj.hora_completa;
                dia_da_semana = data_atual.getDay();
                dia_mes_do_ano = data_atual.getMonth();

                dormiu_posicionado(qtd_bars, price_open[0], `${data_atual_debug}:${hora_atual_debug}` );
    
                hora_maxima_negociacao(qtd_bars, price_open[0]);
    
                reset_valores_dia(serie_dados[i]);
                
                /*if(qtd_bars == 2) {
                    console.log(`data: ${serie_dados[i].data_obj.data_completa} - hora: ${serie_dados[i].data_obj.hora_completa} - qtd_bars: ${qtd_bars} - price_close[1]: ${price_close[1]} - price_close[2]: ${price_close[2]} - price_close[3]: ${price_close[3]}`);
                }*/
                
                if(!COMPRADO && !VENDIDO) {
                    await se_posicionar(individuo);
                } else {
                    await zerar_posicao();
                }
            }
        }

        resolve("terminou day trade");
    });

    return await individuo_day_trade;
}

const run_populacao_day_trade = async (populacao, serie_dados, data_inicio_analise, data_fim_analise) => {
    const new_populacao_day_trade = new Promise(async (resolve, reject) => {
        
        for(j=0; j<populacao.length; j++) {
            await run_day_trade(populacao[j], serie_dados, data_inicio_analise, data_fim_analise);
            //console.log(`run_day_trade: ${j}`);
            text_log += `run_day_trade: ${j}\n`;
        }
    
        resolve("terminou new populacao day trade");
    });

    return await new_populacao_day_trade;
}

const initialize = () => {
    reset_variaveis_globais();

    price_open = [];
    price_high = [];
    price_low = [];
    price_close = [];

    price_open.reverse();
    price_high.reverse();
    price_low.reverse();
    price_close.reverse();
    qtd_bars = 0;

    populacao = [];
    neuronios = [];
    qtd_geracao = 0;
}

const get_data = (dados) => {
    price_open.unshift(dados.open);
    if(price_open.length >= periodo_candles+1) {
        price_open.pop();
    }

    price_high.unshift(dados.high);
    if(price_high.length >= periodo_candles+1) {
        price_high.pop();
    }

    price_low.unshift(dados.low);
    if(price_low.length >= periodo_candles+1) {
        price_low.pop();
    }

    price_close.unshift(dados.close);
    if(price_close.length >= periodo_candles+1) {
        price_close.pop();
    }
}

const reset_valores_dia = (dados) => {
    if(dia_atual != dados.data_obj.dia) {
        // console.log('qtd_bars: ',qtd_bars);
        // console.log(price_open);
        // console.log(price_high);
        // console.log(price_low);
        // console.log(price_close);
        // console.log(dados);
        preco_abertura = price_open[0];
        //console.log(price_open[0]);
        qtd_bars = 0;
        dia_atual = dados.data_obj.dia;
        // console.log(`data: ${dados.data_obj.data_completa} / BALANCE: ${BALANCE}`);
    }
}

const se_posicionar_comprado = async (price, stop_loss, stop_gain) => {
    const set_posicao_comprado = new Promise((resolve, reject) => {
        POSICAO = price;
        STOP_LOSS = stop_loss;
        STOP_GAIN = stop_gain;
        COMPRADO = true;
        QTD_TRADES ++;

        if(modo_debug_posicao) {
            console.log(`(comprado) - data: ${data_atual_debug} - hora: ${hora_atual_debug} - price: ${price} - stop_loss: ${stop_loss} - stop_gain: ${stop_gain} - qtd_bars: ${qtd_bars}`);
        }
        
        resolve('comprado');
    });

    return await set_posicao_comprado;
    
}

const se_posicionar_vendido = async (price, stop_loss, stop_gain) => {
    const set_posicao_vendido = new Promise((resolve, reject) => {
        POSICAO = price;
        STOP_LOSS = stop_loss;
        STOP_GAIN = stop_gain;
        VENDIDO = true;
        QTD_TRADES ++;

        if(modo_debug_posicao) {
            console.log(`(vendido) - data: ${data_atual_debug} - hora: ${hora_atual_debug} - price: ${price} - stop_loss: ${stop_loss} - stop_gain: ${stop_gain} - qtd_bars: ${qtd_bars}`);
        }
        
        resolve('vendido');
    });

    return await set_posicao_vendido;
}

const se_posicionar = async (individuo) => {
    
    if(qtd_bars <= min_qtd_bars) {
        return true;
    }

    if(qtd_bars >= max_qtd_bars_posicao) {
        return true;
    }

    for(let i=0, count = 1; i < neuronios[0].length - 1 - qtd_neuronio_entrada_adicional; i++) {
        if(count >= periodo_candles - 1) {
            count = 1;
        }

        if(count <= 1) {
            neuronios[0][i] = price_close[count] - preco_abertura;
            i++;
            neuronios[0][i] = price_close[count] - preco_abertura;
        } else {
            neuronios[0][i] = price_close[count] - preco_abertura;
            i++;
            
            let distancia_entre_candle = (price_close[1] - price_close[count])*(price_close[1] - price_close[count]) + (count-1)*(count-1);
            neuronios[0][i] = Math.pow( distancia_entre_candle , 0.5);
        }

        count ++;
    }

    let index_neuronio_entrada = neuronios[0].length - 1 - qtd_neuronio_entrada_adicional + 1;
    neuronios[0][index_neuronio_entrada] = qtd_bars;

    index_neuronio_entrada ++;

    neuronios[0][index_neuronio_entrada] = (dia_da_semana + 1);

    index_neuronio_entrada ++;

    neuronios[0][index_neuronio_entrada] = (dia_mes_do_ano + 1);

    if(modo_debug_neuronios) {
        console.log(`qtd_bars: ${qtd_bars} - price_close[1]: ${price_close[1]} - price_close[2]: ${price_close[2]} - price_close[3]: ${price_close[3]}`);
        console.log(`data: ${data_atual_debug} - hora: ${hora_atual_debug} - qtd_bars: ${qtd_bars} - neuronios[0]`, neuronios[0]);
    }
    
    for(let l=0; l < neuronios[1].length; l++ ) {
        neuronios[1][l] = await reLU(neuronios[0], individuo[0][l]);
    }
    
    if(modo_debug_neuronios) {
        console.log('neuronios[1]', neuronios[1]);
    }
    

    for(let k=0; k < neuronios[2].length; k++ ) {
        neuronios[2][k] = await reLU(neuronios[1], individuo[1][k]);
    }

    if(modo_debug_neuronios) {
        console.log('neuronios[2]', neuronios[2]);
    }
    
    for(let m=0; m < neuronios[3].length; m++ ) {
        neuronios[3][m] = await reLU(neuronios[2], individuo[2][m]);
    }

    if(modo_debug_neuronios) {
        console.log(`data: ${data_atual_debug} - hora: ${hora_atual_debug} - qtd_bars: ${qtd_bars} - neuronios[3]`, neuronios[3]);
    }

    await escolher_saida(neuronios[3]);
}

const reset_posicao_comprado = () => {
    POSICAO = null;
    STOP_LOSS = null;
    STOP_GAIN = null;
    COMPRADO = false;
}

const reset_posicao_vendido = () => {
    POSICAO = null;
    STOP_LOSS = null;
    STOP_GAIN = null;
    VENDIDO = false;
}

const close_posicao = (price_posicao, data) => {
    
    if(COMPRADO && VENDIDO) {
        console.log(`Erro :${data} está comprado e vendido ao mesmo tempo`);
    }

    if(COMPRADO) {
        let profit = ((price_posicao - POSICAO)/5) - desconto_por_trade;
        
        if(modo_debug_profit) {
            console.log(`(comprado) - data: ${data_atual_debug} - hora: ${hora_atual_debug} - price: ${price_posicao} - price_open[0]: ${price_open[0]}  - price_open[1]: ${price_open[1]} - price_close[0]: ${price_close[0]}  - price_close[1]: ${price_close[1]} - profit: ${profit} - qtd_bars: ${qtd_bars}`);
        }
        
        if(profit <= 0) {
            QTD_ERROS ++;
            QTD_ERROS_VENDIDO ++;
            PERDA_BRUTA += profit;
        }

        if(profit  > 0) {
            QTD_ACERTOS ++;
            QTD_ACERTOS_VENDIDO ++;
            LUCRO_BRUTO += profit;
        }

        BALANCE += profit - custo_por_trader;
        reset_posicao_comprado();
    }
    
    if(VENDIDO) {
        const profit = ((price_posicao - POSICAO)*(-1))/5 - desconto_por_trade;
        
        if(modo_debug_profit) {
            console.log(`(vendido) - data: ${data_atual_debug} - hora: ${hora_atual_debug} - price: ${price_posicao} - price_open[0]: ${price_open[0]}  - price_open[1]: ${price_open[1]} - price_close[0]: ${price_close[0]}  - price_close[1]: ${price_close[1]} - profit: ${profit} - qtd_bars: ${qtd_bars}`);
        }
        
        if(profit <= 0) {
            QTD_ERROS ++;
            QTD_ERROS_COMPRADO ++;
            PERDA_BRUTA += profit;
        }

        if(profit  > 0) {
            QTD_ACERTOS ++;
            QTD_ACERTOS_COMPRADO ++;
            LUCRO_BRUTO += profit;
        }

        BALANCE += profit - custo_por_trader;
        reset_posicao_vendido();
    }
}

const zerar_posicao = () => {
    if(COMPRADO) {
        if(
            COMPRADO &&
            price_high[1] >= STOP_GAIN
            //price_close[1] >= STOP_GAIN
        ) {
            //close_posicao(price_open[0]);
            STOP_LOSS = price_low[1] - (price_high[1] - price_low[1]);
            STOP_GAIN = price_close[1] + (price_high[1] - price_low[1]);
        }

        if(
            COMPRADO &&
            price_close[1] <= STOP_LOSS
        ) {
            close_posicao(price_open[0]);
        }
    }

    if(VENDIDO) {
        if(
            VENDIDO &&
            price_low[1] <= STOP_GAIN
            //price_close[1] <= STOP_GAIN
        ) {
            //close_posicao(price_open[0]);
            STOP_LOSS = price_high[1] + (price_high[1] - price_low[1]);
            STOP_GAIN = price_close[1] - (price_high[1] - price_low[1]);
        }

        if(
            VENDIDO &&
            price_close[1] >= STOP_LOSS
        ) {
            close_posicao(price_open[0]);
        }
    }
}

const hora_maxima_negociacao = (qtd_bar, price) => {
    if(qtd_bar >= max_qtd_bars_posicao && (COMPRADO || VENDIDO)) {
        close_posicao(price);
    }
}

const dormiu_posicionado = (qtd_bar, price, data) => {
    if(qtd_bar <= min_qtd_bars && (COMPRADO || VENDIDO)) {
        console.log(`Erro : ${data} dormiu posicionado`);
        close_posicao(price);
        //reset_posicao_comprado();
        //reset_posicao_vendido();
    }
}

const reset_variaveis_globais = () => {
    if(COMPRADO) {
        //console.log(`Erro : reset_variaveis_globais comprado`);
        text_log += `Erro : reset_variaveis_globais comprado\n`;
    }

    if(VENDIDO) {
        //console.log(`Erro : reset_variaveis_globais vendido`);
        text_log += `Erro : reset_variaveis_globais vendido\n`;
    }

    COMPRADO = false;
    VENDIDO = false;
    POSICAO = null;
    STOP_LOSS = null;
    STOP_GAIN = null;
    BALANCE = 0;
    QTD_ERROS = 0;
    QTD_ACERTOS = 0;
    LUCRO_BRUTO = 0;
    PERDA_BRUTA = 0;
    QTD_TRADES = 0;
    QTD_ACERTOS_COMPRADO = 0;
    QTD_ERROS_COMPRADO = 0;
    QTD_ACERTOS_VENDIDO = 0;
    QTD_ERROS_VENDIDO = 0;
}

const get_id_saida_mais_ativada = async(saidas) => {
    const res_id = new Promise((resolve, reject) => {
        let index_saida = 0;
        let maior_valor = saidas[0];

        for(let i = 0; i < saidas.length; i++) {
            if(maior_valor <= saidas[i]) {
                index_saida = i;
                maior_valor = saidas[i];
            }
        }

        resolve(index_saida);
    });

    return await res_id;
}

const escolher_saida = async(saidas) => {

    const id_saida = await get_id_saida_mais_ativada(saidas);
    // console.log(`id: ${id_saida}`);

    switch(id_saida) {
    
        case 0: {
            se_posicionar_comprado(price_close[1], price_low[1] - (price_high[1] - price_low[1]) , price_close[1] + (price_close[1] - price_low[1])*2); 
        } break;

        case 1: {
            se_posicionar_comprado(price_close[1], price_low[1] - (price_high[1] - price_low[1])*2 , price_close[1] + (price_close[1] - price_low[1])*2); 
        } break;

        case 2: {
            se_posicionar_comprado(price_close[1], price_low[1] - (price_high[1] - price_low[1])*3 , price_close[1] + (price_close[1] - price_low[1])*2); 
        } break;

        case 3: {
            se_posicionar_comprado(price_close[1], price_low[1] - (price_high[1] - price_low[1]) , price_close[1] + (price_close[1] - price_low[1])*4); 
        } break;

        case 4: {
            se_posicionar_comprado(price_close[1], price_low[1] - (price_high[1] - price_low[1])*2 , price_close[1] + (price_close[1] - price_low[1])*4); 
        } break;

        case 5: {
            se_posicionar_comprado(price_close[1], price_low[1] - (price_high[1] - price_low[1])*3 , price_close[1] + (price_close[1] - price_low[1])*4); 
        } break;

        case 6: {
            se_posicionar_vendido(price_close[1], price_high[1] + (price_high[1] - price_low[1]), price_close[1] - (price_high[1] - price_low[1])*2);
        } break;

        case 7: {
            se_posicionar_vendido(price_close[1], price_high[1] + (price_high[1] - price_low[1])*2, price_close[1] - (price_high[1] - price_low[1])*2);
        } break;

        case 8: {
            se_posicionar_vendido(price_close[1], price_high[1] + (price_high[1] - price_low[1])*3, price_close[1] - (price_high[1] - price_low[1])*2);
        } break;

        case 9: {
            se_posicionar_vendido(price_close[1], price_high[1] + (price_high[1] - price_low[1]), price_close[1] - (price_high[1] - price_low[1])*4);
        } break;

        case 10: {
            se_posicionar_vendido(price_close[1], price_high[1] + (price_high[1] - price_low[1])*2, price_close[1] - (price_high[1] - price_low[1])*4);
        } break;

        case 11: {
            se_posicionar_vendido(price_close[1], price_high[1] + (price_high[1] - price_low[1])*3, price_close[1] - (price_high[1] - price_low[1])*4);
        } break;

        case 12: {
            //console.log("Calma, observe mais !!!!!")
        } break;
    }
}

http.createServer(async (req, res) => {
  qtd_epoca = 0;
  let geracao_geral = await start([]);
  
  const result = {
    qtd_geracao: qtd_max_geracao,
    data: `${data_inicial_treino} ate ${data_final_treino}`,
    resultado: geracao_geral[0].result
  };

  res.writeHead(200, {'Content-Type': 'application/json'});
  res.end(JSON.stringify(result));
}).listen(8001); 

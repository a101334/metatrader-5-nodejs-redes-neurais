#property copyright "Copyright 2020, Alex Rafael da Costa"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Includes                                                         |
//+------------------------------------------------------------------+
#include <Trade\Trade.mqh>
//+------------------------------------------------------------------+
//| Inputs                                                           |
//+------------------------------------------------------------------+
input int qtd_lote = 1;
int lote = qtd_lote;
input ENUM_TIMEFRAMES periodo_timeframe = PERIOD_M5;  
input ENUM_TIMEFRAMES periodo_timeframe_dia = PERIOD_D1;
int periodo_candle = 6;
int qtd_neuronio_entrada_adicional = 3;
input double desvio_padrao = 2;

input int hora_inicio_normal = 9;
input int minuto_inicio_normal = 0;
input int hora_fechamento_normal = 17; 
input int minuto_fechamento_normal = 55;

input int hora_maxima_abrir_posicao = 17;
input int minuto_maximo_abrir_posicao = 50;

input int hora_fechamento_pregao = 17;
input int minuto_fechamento_pregao = 50;

input int dia_min_operar = 1;
input int dia_max_operar = 31;

input bool trabalhar_segunda = true;
input bool trabalhar_terca = true;
input bool trabalhar_quarta = true;
input bool trabalhar_quinta = true;
input bool trabalhar_sexta = true;

input bool trabalhar_janeiro = true;
input bool trabalhar_fevereiro = true;
input bool trabalhar_marco = true;
input bool trabalhar_abril = true;
input bool trabalhar_maio = true;
input bool trabalhar_junho = true;
input bool trabalhar_julho = true;
input bool trabalhar_agosto = true;
input bool trabalhar_setembro = true;
input bool trabalhar_outubro = true;
input bool trabalhar_novembro = true;
input bool trabalhar_dezembro = true;

input double perda_maxima_dia_por_contrato = -3000;
input double fator_ganho_maximo_dia_por_contrato = 2;
input bool utilizar_lote_dinamico = false;
input double maximo_rebaixamento_da_estrategia = 600;
input int min_qtd_bars = 1;
input int max_qtd_bars_posicao = 94;

int qtd_n_entradas = (periodo_candle - 1)*2 + qtd_neuronio_entrada_adicional;
int qtd_n_rede_camada_escondida_1 = (int)NormalizeDouble(qtd_n_entradas*2, 0);
int qtd_n_rede_camada_escondida_2 = (int)NormalizeDouble(qtd_n_entradas*2, 0);
int redes_saidas = 13;

double entrada[][14] = {{718,-69,1149,655,-773,775,-838,-1573,-168,20,715,-45,-344,1267},{1134,1042,288,1240,204,-268,-174,1323,894,1470,-1026,855,-891,775},{-39,-655,-264,384,1008,-617,-216,1602,-82,-517,442,-402,-1254,-381},{-79,115,-347,-1093,96,-653,274,-1178,1163,-18,752,-934,-872,-663},{374,674,-438,-981,-278,-527,173,-414,-333,701,-495,-1802,-1247,-656},{122,-63,-148,-851,-1197,649,-12,-1619,-421,834,443,1292,-348,1061},{-242,-455,-943,974,-167,504,-2004,-454,756,-424,-458,-1062,-822,830},{410,1524,-1062,-980,1013,-66,-199,-413,742,945,-337,512,-1411,-235},{-254,1070,289,-788,-138,261,63,31,-277,-289,-257,605,804,367},{213,-918,931,-627,1418,-567,-488,-247,43,452,-963,-873,306,-531},{-595,40,-284,-100,-377,228,-274,331,-814,496,754,-501,-35,141},{148,866,-68,562,-839,-1375,-819,-56,-940,287,1226,-116,-1890,167},{247,-421,1035,83,1,-973,1226,-505,91,-386,924,208,840,69},{1160,870,619,-55,-588,-440,-733,-295,-377,753,492,-1599,-411,-655},{-707,45,1013,-636,-290,1212,609,168,-1412,-685,469,-529,138,779},{309,867,-60,46,-706,-1264,-432,1018,-359,-497,-915,1269,-279,329},{645,390,-1126,342,-1816,-105,-36,-93,-168,66,1566,-1632,1336,1235},{1471,572,-305,-318,-662,295,58,-981,-313,1327,-1292,-593,-657,-429},{154,-561,106,112,1267,981,859,-163,-977,1027,-788,-349,545,1401},{-292,241,-552,1491,-976,1581,-402,-274,-409,543,-1119,1053,-487,473},{101,-1006,-602,-1543,-322,-129,433,-259,1490,-278,-699,-1397,-651,-851},{-970,376,-1232,-73,1205,708,594,151,1152,112,527,-724,553,179},{1085,-481,506,795,1004,-161,448,-1187,-970,432,662,-1420,307,1207},{-1250,-300,-1804,611,-1399,791,66,-565,506,-186,-763,377,-332,-1777},{112,-1105,1167,-108,119,256,828,-58,300,-376,424,482,145,-324},{767,-390,152,302,965,149,928,-635,1399,-1046,-666,-928,-1445,382},{-127,-970,876,299,252,-832,857,624,-730,414,185,-767,-640,-817}};
double camada_1[][27] = {{1154,810,-263,866,-247,194,-955,174,708,-275,-484,-1076,241,1318,-289,311,237,-291,238,960,1436,8,-382,-123,-412,-252,-965},{-295,763,325,-317,-246,166,-541,864,-855,43,-248,-219,612,-585,-103,-950,668,27,219,-136,-1356,-820,-130,-196,1108,536,378},{60,474,-607,558,-16,-182,-165,-1289,329,528,249,-1132,-1398,1091,88,630,731,783,-224,-840,-509,-1629,1152,1556,-511,183,-729},{-662,114,-82,-1233,861,364,-268,-911,-627,-431,-424,387,-392,788,-670,80,846,8,428,44,-966,-316,567,-447,-413,-735,236},{790,701,-174,593,282,614,-924,671,224,418,312,-281,164,613,-57,497,-328,1290,-599,-584,-890,670,-881,648,382,-542,941},{-75,1355,489,-693,-546,778,-1238,-222,20,1442,296,672,-447,-725,-347,-838,356,449,-1059,-445,-540,-718,1236,229,-281,1150,-383},{-160,926,99,820,329,1391,-342,-17,134,-749,-503,-736,-78,-1283,-258,148,-880,-615,-182,-175,-279,793,-245,529,184,-365,-107},{-519,817,1,-1458,537,-346,-825,-1517,455,625,-368,60,130,-1082,376,464,-572,225,-1182,-566,249,-1076,162,-486,-827,581,43},{160,222,187,36,240,395,1233,-81,73,-315,-271,-366,48,809,15,1265,-863,-1340,-322,-400,367,16,-473,-1076,225,-479,-392},{-703,-297,-398,1110,-311,1430,666,-1189,530,-603,-120,50,321,688,-910,-894,-582,-534,565,1036,-74,-853,-225,-504,-1346,-545,-333},{524,-1369,-1242,-170,-1083,319,212,979,749,-480,-244,622,538,699,318,1106,-23,1874,-1,1179,-1282,-567,459,197,-85,883,188},{110,1074,-15,-1020,-566,103,-568,85,1029,-505,275,65,-355,-189,-493,-633,-627,216,11,-1193,368,-861,144,-289,1441,254,-170},{-267,22,852,701,-316,-915,547,865,1567,245,-148,879,-630,782,640,532,642,-595,570,-994,-419,-1244,-1064,641,-836,-768,885},{310,-577,1300,-947,1744,1499,-434,-359,338,-537,-1211,256,320,48,-279,490,126,469,-1256,436,706,593,349,166,606,-1005,461},{-1346,678,747,-561,91,-799,-759,-492,1601,432,-32,630,606,532,444,-41,916,-542,-517,-396,253,-1506,-446,912,588,99,418},{-1038,1351,99,604,-736,-910,-574,1973,-1341,882,1194,-655,554,387,35,976,1044,1117,-341,578,-1550,1379,538,-208,1545,-485,-161},{70,313,1105,-1456,338,-1200,1798,853,665,-508,-585,170,467,442,-908,-605,-987,1,1001,-39,-756,563,-28,-581,160,534,-397},{-316,1244,1314,-1766,999,1286,868,-1180,-286,94,-612,-870,512,-547,390,-302,-1733,-943,1016,1473,-447,-400,315,-390,872,-689,-953},{-712,743,-791,-317,-378,1050,94,484,-682,-923,806,-694,-694,-160,201,-36,0,580,-1002,399,480,-334,-954,65,-495,500,28},{739,1218,-34,-487,-1256,466,-400,1490,-170,613,-768,-157,-1468,-287,1244,-1042,-1784,881,467,182,276,-144,-341,-1665,228,-326,18},{148,128,-656,-863,-884,-1067,470,-791,453,300,-229,385,674,256,-671,1368,816,-1106,549,-289,-1132,7,508,-273,740,383,-540},{-102,453,-1424,644,696,936,377,-961,1056,455,162,460,161,-900,133,-826,-504,-613,1462,454,257,-608,-1022,-1168,541,-695,1303},{-128,721,963,624,396,-1168,333,222,-26,1177,149,-162,-202,-148,894,-332,150,288,618,-615,349,120,402,1295,-85,491,-1036},{934,-36,-73,234,1931,152,-752,685,-270,231,-372,1058,-571,-907,2058,97,290,482,-576,747,-1213,-52,688,-35,1290,-962,831},{1020,1680,-291,-939,357,214,-391,-25,28,131,510,1113,-585,696,-984,462,796,-985,-1507,149,999,399,-222,705,183,-951,-826},{-256,-385,912,-177,-855,-539,-355,2,-33,657,-441,-216,-781,-5,33,-45,413,-991,738,-722,-243,-530,713,-1152,-1162,409,616},{-745,-106,-394,964,-352,-886,1152,-591,969,135,-486,-609,811,1348,101,-4,692,-366,824,-1100,-590,240,-1081,-1148,-182,-157,763}};
double camada_2[][27] = {{613,503,742,-753,-392,-475,-504,242,495,440,228,1554,976,2219,-342,-1069,-36,-300,11,1559,319,372,184,239,-516,-522,-726},{-1368,76,19,-1121,954,1096,-719,263,105,-333,736,732,-277,-325,48,52,-463,-768,214,-983,-290,48,-523,-1126,-830,583,1287},{305,803,611,-319,1157,-454,77,-1041,721,353,87,-77,3,-580,-927,-1314,75,350,-642,-1370,-547,-260,379,203,230,409,-1266},{-730,-291,-862,313,842,-783,301,2004,231,-425,796,-400,-215,-308,-488,-1013,35,85,-783,258,-639,1388,-682,833,-171,1211,-358},{-1308,212,-460,-1311,324,1579,-176,-732,389,782,36,67,-10,1644,1125,1298,181,76,504,214,-215,266,37,400,-919,1293,-874},{-453,-318,275,1308,1035,-833,1360,-974,716,408,-234,207,-876,-865,774,-472,-574,1428,-1863,440,823,-170,217,342,-40,426,-233},{-188,-1377,289,-685,-154,1065,-427,98,-1406,-1321,-434,-1092,309,-597,367,-31,-813,-713,-383,-207,1167,-625,-232,955,1447,482,-584},{828,-879,-343,677,1516,-577,1337,441,-261,394,-497,1359,-255,-255,-768,967,367,-498,-1088,610,-582,1136,-480,59,-1407,697,-476},{248,-771,-825,1031,988,-429,-499,462,272,-195,116,456,603,1046,224,218,-116,621,411,-877,269,-1581,1085,496,362,1567,452},{372,98,662,502,180,157,296,78,54,-530,-82,-506,749,693,1685,292,-467,1282,-426,-331,663,559,-1950,1506,980,-238,-580},{118,35,-737,105,419,219,-47,-585,137,960,-1286,-740,-686,-304,448,831,58,605,-120,-379,-3,-2,-439,479,-701,434,1311},{939,-253,-623,-232,989,-789,318,286,-753,691,-66,-808,-255,991,-1107,1346,-294,-298,513,639,-458,410,-256,622,312,-1180,1087},{-435,-642,-289,99,40,63,-1478,1477,954,-1111,1244,-761,102,629,1557,550,75,261,-226,257,-779,-136,-410,1425,1096,248,209},{-1263,-358,-247,-595,-714,119,-956,426,-90,85,313,1219,-473,-719,1875,-386,-407,342,-374,66,747,405,-334,1369,119,-485,140}};

double neuronio_entrada[][14] = {0,0,0,0,0,0,0,0,0,0,0,0,0,1};
double neuronio_camada_1[][27] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1};
double neuronio_camada_2[][27] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1};
double neuronio_saida[][13] = {0,0,0,0,0,0,0,0,0,0,0,0,0};
//+------------------------------------------------------------------+
//| Enums                                                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Variaveis globais                                                |
//+------------------------------------------------------------------+
CTrade trade;

double price_open [];
double price_high [];
double price_low [];
double price_close [];

double price_open_dia [];
double price_high_dia [];
double price_low_dia [];
double price_close_dia [];

bool comprado = false;
bool vendido = false;
ulong tick_comprado = 0;
ulong tick_vendido = 0;

ulong magic_number = 12345;

int qtd_bars_dia = 0;
int qtd_gains_dia = 0;
int qtd_loss_dia = 0;
int posicao_maxima_dia = 0;
int posicao_minima_dia = 0;

double maxima_dia = 0;
double minima_dia = 0;

double perda_maxima_dia = lote*perda_maxima_dia_por_contrato;
double ganho_maxima_dia = -1*lote*perda_maxima_dia_por_contrato*fator_ganho_maximo_dia_por_contrato;
double tamanho_medio_candles = 0;
double stop_loss_atual_pontos = 0;
double stop_gain_atual_pontos = 0;

double tick_size = 0;

MqlDateTime hora_atual, hora_servidor, hora_ajuste_qtd_bars_dia;

int dia_atual = 0;
int mes_atual = 0;
int dia_semana = -1;
double balanco_atual = 0;

string hl_stop_loss_atual = "hl_stop_loss_atual";
string hl_stop_gain_atual = "hl_gain_loss_atual";

string hl_suporte_atual = "hl_suporte_atual";
string hl_resistencia_atual = "hl_resistencia_atual";
string hl_suporte_anterior = "hl_suporte_anterior";
string hl_resistencia_anterior = "hl_resistencia_anterior";

int OnInit()
  {
//---
   remove_all_indicator();
   
   if(!inicializar_indicadores()){
      Print("Erro ao inicializar o sistema");
      return(INIT_FAILED);
   }
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   remove_all_indicator();
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
//---
   reset_valores_dia();
   
   if(verifica_posicao()) {
      hora_maxima_negociacao_zera_posicao();
   }
   
   if(is_new_bar()) {

      qtd_bars_dia ++;

      if(get_data()){
         calcula_tamanho_medio_candles();
         
         show_comentarios();
         
         if(!hora_negociacao()) {
            return;
         }
         
         if(verifica_posicao()) {
            
            if(!comprado && !vendido) {
               if(!logica_trade()) {
                  Print("Error: logica_trade");
               }
            } else {
               zerar_posicao();
            }
            
         }
      }
   } 
}
//+------------------------------------------------------------------+

bool is_new_bar(){
//--- memorize the time of opening of the last bar in the static variable
   static datetime last_time=0;
//--- current time
   datetime lastbar_time=(datetime)SeriesInfoInteger(_Symbol, periodo_timeframe, SERIES_LASTBAR_DATE);

//--- if it is the first call of the function
   if(last_time==0)
     {
      //--- set the time and exit
      last_time=lastbar_time;
      return(false);
     }

//--- if the time differs
   if(last_time!=lastbar_time)
     {
      //--- memorize the time and return true
      last_time=lastbar_time;
      return(true);
     }
//--- if we passed to this line, then the bar is not new; return false
   return(false);
}

bool inicializar_indicadores () {
   TimeToStruct(TimeCurrent(), hora_servidor);
   string hora_servidor_string = (string)hora_servidor.day+"/"+(string)hora_servidor.mon+"/"+(string)hora_servidor.year+" - "+(string)hora_servidor.hour+":"+(string)hora_servidor.min;
   
   Print("Hora Servidor: ", hora_servidor_string);
   
   tick_size = SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_SIZE);
   
   if(tick_size <= 0){
      Print("Error ao carregar a informação do tick_size");
      return false;
   }
   
   if(!ArraySetAsSeries(price_open, true)){
      Print("Error ao inverter o array price_open");
      return false;
   }
   
   if(!ArraySetAsSeries(price_high, true)){
      Print("Error ao inverter o array price_high");
      return false;
   }
   
   if(!ArraySetAsSeries(price_low, true)){
      Print("Error ao inverter o array price_low");
      return false;
   }
   
   if(!ArraySetAsSeries(price_close, true)){
      Print("Error ao inverter o array price_close");
      return false;
   }
   
   //candle dia
   if(!ArraySetAsSeries(price_open_dia, true)){
      Print("Error ao inverter o array price_open_dia");
      return false;
   }
   
   if(!ArraySetAsSeries(price_high_dia, true)){
      Print("Error ao inverter o array price_high_dia");
      return false;
   }
   
   if(!ArraySetAsSeries(price_low_dia, true)){
      Print("Error ao inverter o array price_low_dia");
      return false;
   }
   
   if(!ArraySetAsSeries(price_close_dia, true)){
      Print("Error ao inverter o array price_close_dia");
      return false;
   }
   
   trade.SetExpertMagicNumber(magic_number);
   
   if( !plotar_todas_linhas() ) {
      return false;
   }
   
   return true;
}

bool get_data() {
   int price_open_copy = CopyOpen(_Symbol, periodo_timeframe, 0, periodo_candle, price_open);
   
   if(price_open_copy != periodo_candle){
      Print("Error capturar os dados do price_open");
      return false;
   }
   
   int price_high_copy = CopyHigh(_Symbol, periodo_timeframe, 0, periodo_candle, price_high);
   
   if(price_high_copy != periodo_candle){
      Print("Error capturar os dados do price_high");
      return false;
   }
   
   int price_low_copy = CopyLow(_Symbol, periodo_timeframe, 0, periodo_candle, price_low);
   
   if(price_low_copy != periodo_candle){
      Print("Error capturar os dados do price_low");
      return false;
   }
   
   int price_close_copy = CopyClose(_Symbol, periodo_timeframe, 0, periodo_candle, price_close);
   
   if(price_close_copy != periodo_candle){
      Print("Error capturar os dados do price_close");
      return false;
   }
   
   //candles dia
   int price_open_dia_copy = CopyOpen(_Symbol, periodo_timeframe_dia, 0, periodo_candle, price_open_dia);
   
   if(price_open_dia_copy != periodo_candle){
      Print("Error capturar os dados do price_open_dia_");
      return false;
   }
   
   int price_high_dia_copy = CopyHigh(_Symbol, periodo_timeframe_dia, 0, periodo_candle, price_high_dia);
   
   if(price_high_dia_copy != periodo_candle){
      Print("Error capturar os dados do price_high_dia");
      return false;
   }
   
   int price_low_dia_copy = CopyLow(_Symbol, periodo_timeframe_dia, 0, periodo_candle, price_low_dia);
   
   if(price_low_dia_copy != periodo_candle){
      Print("Error capturar os dados do price_low_dia");
      return false;
   }
   
   int price_close_dia_copy = CopyClose(_Symbol, periodo_timeframe_dia, 0, periodo_candle, price_close_dia);
   
   if(price_close_dia_copy != periodo_candle){
      Print("Error capturar os dados do price_close_dia");
      return false;
   }
   
   return true;
}

bool verifica_posicao (){
   comprado = false;
   vendido = false;
   tick_comprado = 0;
   tick_vendido = 0;
   
   if(PositionSelect(_Symbol)) {
      if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY) {
         tick_comprado = PositionGetInteger(POSITION_TICKET);
         comprado = true;
      }
      
      if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL) {
         tick_vendido = PositionGetInteger(POSITION_TICKET);
         vendido = true;
      }
   }
  
   return true;
}

bool se_posicionar () {
   
   TimeToStruct(TimeCurrent(), hora_ajuste_qtd_bars_dia);
   
   if(hora_ajuste_qtd_bars_dia.hour == 9 && hora_ajuste_qtd_bars_dia.min == 5) {
      qtd_bars_dia = 1;
   }
   
   if(balanco_atual >= ganho_maxima_dia) {
      return true;
   }
   
   if(balanco_atual <= perda_maxima_dia) {
      return true;
   }
   
   if(hora_maxima_negociacao ()) {
      return true;
   }
   
   if(tempo_maximo_abrir_posicao()) {
      return true;
   }
   
   if(comprado || vendido) {
      return true;
   }
   
   if(qtd_bars_dia <= min_qtd_bars) {
      return true;
   }
   
   if(qtd_bars_dia >= max_qtd_bars_posicao) {
      return true;
   }
   
   reset_posicao();
   
   /*Print("neuronio_entrada[0][i]: ", DoubleToString(neuronio_entrada[0][0], 0));
   Print("neuronio_camada_1[0][i]: ", DoubleToString(neuronio_camada_1[0][0], 0));
   Print("neuronio_camada_2[0][i]: ", DoubleToString(neuronio_camada_2[0][0], 0));
   Print("neuronio_saida[0][i]: ", DoubleToString(neuronio_saida[0][0], 0));*/
   
   /*Print("entrada[0][i]: ", DoubleToString(entrada[1][0], 0));
   Print("camada_1[0][i]: ", DoubleToString(camada_1[1][0], 0));
   Print("camada_2[0][i]: ", DoubleToString(camada_2[1][0], 0));*/
   
   /*Print("size neuronio_entrada: ", IntegerToString((long)ArraySize(neuronio_entrada)));
   Print("size neuronio_camada_1: ", IntegerToString((long)ArraySize(neuronio_camada_1)));
   Print("size neuronio_camada_2: ", IntegerToString((long)ArraySize(neuronio_camada_2)));
   Print("size neuronio_saida: ", IntegerToString((long)ArraySize(neuronio_saida)));
   
   Print("size entrada: ", IntegerToString((long)ArraySize(entrada)/25));
   Print("size camada_1: ", IntegerToString((long)ArraySize(camada_1)/49));
   Print("size camada_2: ", IntegerToString((long)ArraySize(camada_2)/49));*/
   
   for(int i=0, count = 1; i < ArraySize(neuronio_entrada) - 1 - qtd_neuronio_entrada_adicional; i++) {
        if(count >= periodo_candle - 1) {
            count = 1;
        }
         
        if(count <= 1) {
            neuronio_entrada[0][i] = price_close[count] - price_open_dia[0];
            i++;
            neuronio_entrada[0][i] = price_close[count] - price_open_dia[0];
        } else {
            neuronio_entrada[0][i] = price_close[count] - price_open_dia[0];
            i++;
            
            double distancia_entre_candle = (price_close[1] - price_close[count])*(price_close[1] - price_close[count]) + (count - 1)*(count - 1);
            neuronio_entrada[0][i] = pow( distancia_entre_candle , 0.5);
        }

        count ++;
    }
    
    int index_neuronio_entrada = ArraySize(neuronio_entrada) - 1 - qtd_neuronio_entrada_adicional + 1;
    neuronio_entrada[0][index_neuronio_entrada] = qtd_bars_dia;
    
    index_neuronio_entrada ++;
    
    neuronio_entrada[0][index_neuronio_entrada] = dia_semana;
    
    index_neuronio_entrada ++;
    
    neuronio_entrada[0][index_neuronio_entrada] = mes_atual;
    
    if(qtd_bars_dia == 2) {
      Print("qtd_bars: ", qtd_bars_dia, " - price_close[1] : ", DoubleToString(price_close[1], 0), " - price_close[2] : ", DoubleToString(price_close[2], 0), " - price_close[3] : ", DoubleToString(price_close[3], 0) );
    }
    
    /*
    Print("============== neuronio_entrada =====================");
    ArrayPrint(neuronio_entrada);
    Print("=====================================================");
    */
    
    for(int l=0; l < ArraySize(neuronio_camada_1); l++ ) {
        //neuronio_camada_1[0][l] = reLU(neuronio_entrada, entrada, l);
        
        double total = 0;

        for(int i=0; i< ArraySize(neuronio_entrada); i++) {
            total += neuronio_entrada[0][i]*entrada[l][i];
        }
      
        if(total <= 0) {
            neuronio_camada_1[0][l] = 0.01;
            continue;
        }
      
        neuronio_camada_1[0][l] = NormalizeDouble((total/1000), 0);
    }
    
    //Print("============== neuronio_camada_1 =====================");
    //ArrayPrint(neuronio_camada_1);
    //Print("=====================================================");
    
    for(int k=0; k < ArraySize(neuronio_camada_2); k++ ) {
        //neuronio_camada_2[0][k] = reLU(neuronio_camada_1, camada_1, k);
        
        double total = 0;

        for(int i=0; i< ArraySize(neuronio_camada_1); i++) {
            total += neuronio_camada_1[0][i]*camada_1[k][i];
        }
      
        if(total <= 0) {
            neuronio_camada_2[0][k] = 0.01;
            continue;
        }
      
        neuronio_camada_2[0][k] = NormalizeDouble((total/1000), 0);
    }
    
    //Print("============== neuronio_camada_2 =====================");
    //ArrayPrint(neuronio_camada_2);
    //Print("=====================================================");
    
    for(int m=0; m < ArraySize(neuronio_saida); m++ ) {
        //neuronio_saida[0][m] = reLU(neuronio_camada_2, camada_2, m);
        
        double total = 0;

        for(int i=0; i< ArraySize(neuronio_camada_2); i++) {
            total += neuronio_camada_2[0][i]*camada_2[m][i];
        }
      
        if(total <= 0) {
            neuronio_saida[0][m] = 0.01;
            continue;
        }
      
        neuronio_saida[0][m] = NormalizeDouble((total/1000), 0);
    }
    
    Print("============== neuronio_saida =====================");
    ArrayPrint(neuronio_saida);
    Print("=====================================================");
    
    int id_saida = 0;
     double maior_valor = neuronio_saida[0][0];

     for(int i = 0; i < ArraySize(neuronio_saida); i++) {
         if(maior_valor <= neuronio_saida[0][i]) {
             id_saida = i;
             maior_valor = neuronio_saida[0][i];
         }
     }
    
    //Print("id_saida: ", id_saida," - qtd_bars_dia: ", qtd_bars_dia);
    
    switch(id_saida) {
    
        case 0: {
            se_posicionar_comprado(tamanho_medio_candles, price_low[1] - (price_high[1] - price_low[1]) , price_close[1] + (price_close[1] - price_low[1])*2); 
        } break;

        case 1: {
            se_posicionar_comprado(tamanho_medio_candles, price_low[1] - (price_high[1] - price_low[1])*2 , price_close[1] + (price_close[1] - price_low[1])*2); 
        } break;

        case 2: {
            se_posicionar_comprado(tamanho_medio_candles, price_low[1] - (price_high[1] - price_low[1])*3 , price_close[1] + (price_close[1] - price_low[1])*2); 
        } break;

        case 3: {
            se_posicionar_comprado(tamanho_medio_candles, price_low[1] - (price_high[1] - price_low[1]) , price_close[1] + (price_close[1] - price_low[1])*4); 
        } break;

        case 4: {
            se_posicionar_comprado(tamanho_medio_candles, price_low[1] - (price_high[1] - price_low[1])*2 , price_close[1] + (price_close[1] - price_low[1])*4); 
        } break;

        case 5: {
            se_posicionar_comprado(tamanho_medio_candles, price_low[1] - (price_high[1] - price_low[1])*3 , price_close[1] + (price_close[1] - price_low[1])*4); 
        } break;

        case 6: {
            se_posicionar_vendido(tamanho_medio_candles, price_high[1] + (price_high[1] - price_low[1]), price_close[1] - (price_high[1] - price_low[1])*2);
        } break;

        case 7: {
            se_posicionar_vendido(tamanho_medio_candles, price_high[1] + (price_high[1] - price_low[1])*2, price_close[1] - (price_high[1] - price_low[1])*2);
        } break;

        case 8: {
            se_posicionar_vendido(tamanho_medio_candles, price_high[1] + (price_high[1] - price_low[1])*3, price_close[1] - (price_high[1] - price_low[1])*2);
        } break;

        case 9: {
            se_posicionar_vendido(tamanho_medio_candles, price_high[1] + (price_high[1] - price_low[1]), price_close[1] - (price_high[1] - price_low[1])*4);
        } break;

        case 10: {
            se_posicionar_vendido(tamanho_medio_candles, price_high[1] + (price_high[1] - price_low[1])*2, price_close[1] - (price_high[1] - price_low[1])*4);
        } break;

        case 11: {
            se_posicionar_vendido(tamanho_medio_candles, price_high[1] + (price_high[1] - price_low[1])*3, price_close[1] - (price_high[1] - price_low[1])*4);
        } break;

        case 12: {
            //console.log("Calma, observe mais !!!!!")
        } break;
    }
   
   return true;
}

bool logica_trade() {
   if(comprado || vendido) {
   
   } else {
      if(!se_posicionar ()){
         Print("Error: se_posicionar");
         return false;
      }
   }
   
   return true;
}

void reset_valores_dia() {
   TimeToStruct(TimeCurrent(), hora_atual);
   
   if(dia_atual != hora_atual.day) {
      dia_atual = hora_atual.day;
      dia_semana = hora_atual.day_of_week;
      mes_atual = hora_atual.mon;
      comprado = false;
      vendido = false;
      tick_comprado = 0;
      tick_vendido = 0;
      qtd_bars_dia = 0;
      stop_loss_atual_pontos = 0;
      stop_gain_atual_pontos = 0;
     
      qtd_gains_dia = 0;
      qtd_loss_dia = 0;
      
      balanco_atual = 0;
      
      reset_posicao();
      
      if(mes_atual != hora_atual.mon && utilizar_lote_dinamico) {
         
         if( AccountInfoDouble(ACCOUNT_BALANCE) <= 25000 ) {
            lote = (int)NormalizeDouble(AccountInfoDouble(ACCOUNT_BALANCE)/maximo_rebaixamento_da_estrategia, 0);
         }
         
         if( AccountInfoDouble(ACCOUNT_BALANCE) > 25000 && AccountInfoDouble(ACCOUNT_BALANCE) <= 50000 ) {
            lote = (int)NormalizeDouble(AccountInfoDouble(ACCOUNT_BALANCE)/(maximo_rebaixamento_da_estrategia + 500), 0);
         }
         
         if( AccountInfoDouble(ACCOUNT_BALANCE) > 50000 && AccountInfoDouble(ACCOUNT_BALANCE) <= 100000 ) {
            lote = (int)NormalizeDouble(AccountInfoDouble(ACCOUNT_BALANCE)/(maximo_rebaixamento_da_estrategia + 1000), 0);
         }
         
         if( AccountInfoDouble(ACCOUNT_BALANCE) > 100000 && AccountInfoDouble(ACCOUNT_BALANCE) <= 200000 ) {
            lote = (int)NormalizeDouble(AccountInfoDouble(ACCOUNT_BALANCE)/(maximo_rebaixamento_da_estrategia + 1500), 0);
         }
         
         if( AccountInfoDouble(ACCOUNT_BALANCE) > 250000 ) {
            lote = (int)NormalizeDouble(AccountInfoDouble(ACCOUNT_BALANCE)/(maximo_rebaixamento_da_estrategia*3), 0);
         }
         
         if(lote >= 500) {
            lote = 500;
         }
         
         perda_maxima_dia = lote*perda_maxima_dia_por_contrato;
         ganho_maxima_dia = -1*lote*perda_maxima_dia_por_contrato*fator_ganho_maximo_dia_por_contrato;
      }
   }
   
}

double normalize_pontos (double pontos) {
   return NormalizeDouble(pontos/tick_size, 0)*tick_size;
}

bool hora_negociacao () {
    TimeToStruct(TimeCurrent(), hora_atual);
    
    if (hora_atual.day_of_week == 0 || hora_atual.day_of_week == 6) {
      return false;
    }
    
    //Meses do ano
    if(hora_atual.mon == 1 && !trabalhar_janeiro) {
      return false;
    }
   
    if(hora_atual.mon == 2 && !trabalhar_fevereiro) {
      return false;
    }

    if(hora_atual.mon == 3 && !trabalhar_marco) {
      return false;
    }
    
    if(hora_atual.mon == 4 && !trabalhar_abril) {
      return false;
    }
    
    if(hora_atual.mon == 5 && !trabalhar_maio) {
      return false;
    }
    
    if(hora_atual.mon == 6 && !trabalhar_junho) {
      return false;
    }
    
    if(hora_atual.mon == 7 && !trabalhar_julho) {
      return false;
    }
    
    if(hora_atual.mon == 8 && !trabalhar_agosto) {
      return false;
    }
    
    if(hora_atual.mon == 9 && !trabalhar_setembro) {
      return false;
    }
    
    if(hora_atual.mon == 10 && !trabalhar_outubro) {
      return false;
    }
    
    if(hora_atual.mon == 11 && !trabalhar_novembro) {
      return false;
    }
    
    if(hora_atual.mon == 12 && !trabalhar_dezembro) {
      return false;
    }
    
    //Dias da Semana
    if(hora_atual.day_of_week == 1 && !trabalhar_segunda) {
      return false;
    }
    
    if(hora_atual.day_of_week == 2 && !trabalhar_terca) {
      return false;
    }
    
    if(hora_atual.day_of_week == 3 && !trabalhar_quarta) {
      return false;
    }
    
    if(hora_atual.day_of_week == 4 && !trabalhar_quinta) {
      return false;
    }
    
    if(hora_atual.day_of_week == 5 && !trabalhar_sexta) {
      return false;
    }
    
    //Intervalo de dias do mês
    if(hora_atual.day < dia_min_operar) {
      return false;
    }
    
    if(hora_atual.day > dia_max_operar) {
      return false;
    }
    
    if (hora_atual.hour >= hora_inicio_normal && hora_atual.hour <= hora_fechamento_normal) {
    
       if (hora_atual.hour == hora_inicio_normal) {
         
         if (hora_atual.min >= minuto_inicio_normal) {
            return true;
         }
         
         return false;
       }
       
        if (hora_atual.hour == hora_fechamento_normal) {
         
         if (hora_atual.min <= minuto_fechamento_normal) {
            return true;
         }
         
         return false;
       }
       
       return true;
    }
    
    return false;
}

bool hora_maxima_negociacao () {
    TimeToStruct(TimeCurrent(), hora_atual);
    
    if(hora_atual.hour > hora_fechamento_pregao) {
      return true;
    }
    
    if (hora_atual.hour >= hora_fechamento_pregao && hora_atual.min >= minuto_fechamento_pregao) {
      return true;
    }
    
    if(qtd_bars_dia >= max_qtd_bars_posicao) {
      return true;
    }
    
    return false;
}

bool tempo_maximo_abrir_posicao () {
    TimeToStruct(TimeCurrent(), hora_atual);
    
    if (hora_atual.hour > hora_maxima_abrir_posicao) {
      return true;
    }
    
    if (hora_atual.hour >= hora_maxima_abrir_posicao && hora_atual.min >= minuto_maximo_abrir_posicao) {
      return true;
    }
    
    return false;
}

void hora_maxima_negociacao_zera_posicao() {

  if(hora_maxima_negociacao() && (comprado || vendido)) {
      if(!trade.PositionClose(_Symbol)){
         Print("Error: zerar posicao (hora_maxima_negociacao_zera_posicao)");
         return;
      }
      
      balanco_atual += PositionGetDouble(POSITION_PROFIT);
      Print("Balanço atual: "  ,DoubleToString(balanco_atual, 2));
      Print("posicionado -> zerado (hora_maxima_negociacao_zera_posicao)");
      Print("(hora_maxima) - profit: "  ,DoubleToString(PositionGetDouble(POSITION_PROFIT), 2), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
  }
}

void calcula_tamanho_medio_candles() {
   double soma_total_tamanho_candle = 0;
   
   for(int i = 0; i < periodo_candle; i++) {
      soma_total_tamanho_candle += price_high[i] - price_low[i];
   }
   
   tamanho_medio_candles = soma_total_tamanho_candle/periodo_candle; 
}

bool zerar_posicao() {
   
   if (comprado) {
      // 3° Axioma: Esperança (obs: perceber que o barco está afundado, abandonar logo)
      if(comprado && price_close[1] <= stop_loss_atual_pontos) {
          bool res_pos_close = trade.PositionClose(_Symbol);
          
          if(res_pos_close){
            
            if(trade.ResultRetcode() == 10008 || trade.ResultRetcode() == 10009) {
               balanco_atual += PositionGetDouble(POSITION_PROFIT);
               Print("Balanço atual: "  ,DoubleToString(balanco_atual, 2));
               Print("(comprado) - price_open[0]: ", DoubleToString(price_open[0],0), " - price_open[1]: ", DoubleToString(price_open[1],0) ," - price_close[0]: ", DoubleToString(price_close[0],0) ," - price_close[1]: ", DoubleToString(price_close[1],0) ," - profit: "  ,DoubleToString(PositionGetDouble(POSITION_PROFIT), 2), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
               qtd_loss_dia ++;
               return true;
            }
            
            Print("Error: trade.PositionClose2 - ResultRetcode: ", trade.ResultRetcode());
            return false;
          }
      }
      
      if(comprado && price_close[1] >= stop_gain_atual_pontos) {
          stop_loss_atual_pontos = price_low[1] - (price_high[1] - price_low[1]);
          stop_gain_atual_pontos = price_close[1] + (price_high[1] - price_low[1]);
          
          ObjectSetDouble(0, hl_stop_gain_atual, OBJPROP_PRICE, stop_gain_atual_pontos);
          ObjectSetDouble(0, hl_stop_loss_atual, OBJPROP_PRICE, stop_loss_atual_pontos);

          /*bool res_pos_close = trade.PositionClose(_Symbol);
          if(res_pos_close){

            if(trade.ResultRetcode() == 10008 || trade.ResultRetcode() == 10009) {
               balanco_atual += PositionGetDouble(POSITION_PROFIT);
               Print("Balanço atual: "  ,DoubleToString(balanco_atual, 2));
               Print("(comprado) - price_open[0]: ", DoubleToString(price_open[0],0), " - price_open[1]: ", DoubleToString(price_open[1],0) ," - price_close[0]: ", DoubleToString(price_close[0],0) ," - price_close[1]: ", DoubleToString(price_close[1],0) ," - profit: "  ,DoubleToString(PositionGetDouble(POSITION_PROFIT), 2), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
               qtd_gains_dia ++;
               return true;
            }
            
            Print("Error: trade.PositionClose2 - ResultRetcode: ", trade.ResultRetcode());
            return false;
          }*/
      }
   }
   
   if (vendido) {
      // 3° Axioma: Esperança (obs: perceber que o barco está afundado, abandonar logo)
      if(vendido && price_close[1] >= stop_loss_atual_pontos) {
          bool res_pos_close = trade.PositionClose(_Symbol);
          
          if(res_pos_close){
      
            if(trade.ResultRetcode() == 10008 || trade.ResultRetcode() == 10009) {
               balanco_atual += PositionGetDouble(POSITION_PROFIT);
               Print("Balanço atual: "  ,DoubleToString(balanco_atual, 2));
               Print("(vendido) - price_open[0]: ", DoubleToString(price_open[0],0), " - price_open[1]: ", DoubleToString(price_open[1],0) ," - price_close[0]: ", DoubleToString(price_close[0],0) ," - price_close[1]: ", DoubleToString(price_close[1],0) ," - profit: "  ,DoubleToString(PositionGetDouble(POSITION_PROFIT), 2), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
               qtd_loss_dia ++;
               return true;
            }
            
            Print("Error: trade.PositionClose2 - ResultRetcode: ", trade.ResultRetcode());
            return false;
          }
      }
      
      if(vendido && price_close[1] <= stop_gain_atual_pontos) {
            stop_loss_atual_pontos = price_high[1] + (price_high[1] - price_low[1]);
            stop_gain_atual_pontos = price_close[1] - (price_high[1] - price_low[1]);
            
            ObjectSetDouble(0, hl_stop_gain_atual, OBJPROP_PRICE, stop_gain_atual_pontos);
            ObjectSetDouble(0, hl_stop_loss_atual, OBJPROP_PRICE, stop_loss_atual_pontos);

          /*bool res_pos_close = trade.PositionClose(_Symbol);
          
          if(res_pos_close){
      
            if(trade.ResultRetcode() == 10008 || trade.ResultRetcode() == 10009) {
               balanco_atual += PositionGetDouble(POSITION_PROFIT);
               Print("Balanço atual: "  ,DoubleToString(balanco_atual, 2));
               Print("(vendido) - price_open[0]: ", DoubleToString(price_open[0],0), " - price_open[1]: ", DoubleToString(price_open[1],0) ," - price_close[0]: ", DoubleToString(price_close[0],0) ," - price_close[1]: ", DoubleToString(price_close[1],0) ," - profit: "  ,DoubleToString(PositionGetDouble(POSITION_PROFIT), 2), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
               qtd_gains_dia ++;
               return true;
            }
            
            Print("Error: trade.PositionClose2 - ResultRetcode: ", trade.ResultRetcode());
            return false;
          }*/
      }
   }
 
   return true;
}

void show_comentarios() {
   datetime time  = iTime(Symbol(),periodo_timeframe,0);
             
    Comment (
               "\n\n\n\n\n\n\n\n\n\n\nTime: "  ,TimeToString(time,TIME_MINUTES),"\n",
               "Current Position Profit: "  ,PositionGetDouble(POSITION_PROFIT),"\n",
               "Balanço atual: "  ,DoubleToString(balanco_atual, 2),"\n",
               "Stop Loss atual (pontos)", DoubleToString(stop_loss_atual_pontos, 2), "\n",
               "Stop Gain atual (pontos)", DoubleToString(stop_gain_atual_pontos, 2), "\n",
               "QTD Loss dia", qtd_loss_dia, "\n",
               "QTD Gain dia", qtd_gains_dia, "\n"
            );
}

void remove_all_indicator() {
   int subwindows=(int)ChartGetInteger(0,CHART_WINDOWS_TOTAL);

   for(int i=subwindows;i>=0;i--)
     {
      int indicators=ChartIndicatorsTotal(0,i);

      for(int j=indicators-1; j>=0; j--)
        {
         ChartIndicatorDelete(0,i,ChartIndicatorName(0,i,j));
        }
     }
}

bool plotar_linha_horizontal (string nome_objeto, double posicao_hl, color color_hl, ENUM_LINE_STYLE style_line, int width) { 
   
   if( !ObjectCreate(0, nome_objeto, OBJ_HLINE, 0, 0, posicao_hl) ){
      Print("Problema ao criar a linha horizontal: " + nome_objeto);
      return false;
   }
   
   if( !ObjectSetInteger(0, nome_objeto, OBJPROP_COLOR, color_hl) ){
      Print("Problema ao setar a cor linha horizontal: " + nome_objeto);
      return false;
   }
   
   if( !ObjectSetInteger(0, nome_objeto, OBJPROP_WIDTH, width) ){
      Print("Problema ao setar a espesura linha horizontal: " + nome_objeto);
      return false;
   }
   
   if( !ObjectSetInteger(0, nome_objeto, OBJPROP_STYLE, style_line) ){
      Print("Problema ao setar a espesura linha horizontal: " + nome_objeto);
      return false;
   }
   
   return true;
}

bool plotar_todas_linhas() {
   
   //Linhas Stop Loss, Stop Gain
   if( !plotar_linha_horizontal(hl_stop_loss_atual, 0, clrRed, STYLE_SOLID, 2) ) {
      Print("Problema ao criar a hl_stop_loss_atual");
      return false;
   }
   
   if( !plotar_linha_horizontal(hl_stop_gain_atual, 0, clrGreen, STYLE_SOLID, 2) ) {
      Print("Problema ao criar a hl_gain_loss_atual");
      return false;
   }
   
   //Linhas Suporte e Resistência.
   if( !plotar_linha_horizontal(hl_suporte_atual, 0, clrPink, STYLE_SOLID, 2) ) {
      Print("Problema ao criar a hl_suporte_atual");
      return false;
   }
   
   if( !plotar_linha_horizontal(hl_resistencia_atual, 0, clrPurple, STYLE_SOLID, 2) ) {
      Print("Problema ao criar a hl_resistencia_atual");
      return false;
   }
   
   if( !plotar_linha_horizontal(hl_suporte_anterior, 0, clrPink, STYLE_DASH, 2) ) {
      Print("Problema ao criar a hl_suporte_anterior");
      return false;
   }
   
   if( !plotar_linha_horizontal(hl_resistencia_anterior, 0, clrPurple, STYLE_DASH, 2) ) {
      Print("Problema ao criar a hl_resistencia_anterior");
      return false;
   }
   
   return true;
}

void reset_posicao() {
   stop_gain_atual_pontos = 0;
   stop_loss_atual_pontos = 0;
   
   ObjectSetDouble(0, hl_stop_gain_atual, OBJPROP_PRICE, stop_gain_atual_pontos);
   ObjectSetDouble(0, hl_stop_loss_atual, OBJPROP_PRICE, stop_loss_atual_pontos);
}


bool se_posicionar_comprado(double tamanho_posicao, double stop_loss_posicao, double stop_gain_posicao) {
   
   double stop_loss_ordem = stop_loss_posicao - tamanho_posicao*30;
   double stop_gain_ordem = stop_gain_posicao + tamanho_posicao*30;
   
   bool res_trade_buy = trade.Buy(lote, _Symbol, 0, normalize_pontos(stop_loss_ordem), normalize_pontos(stop_gain_ordem), "zerado->comprado");
   
   if(res_trade_buy){
   
      if(trade.ResultRetcode() == 10008 || trade.ResultRetcode() == 10009) {
    
         stop_loss_atual_pontos = stop_loss_posicao;
         stop_gain_atual_pontos = stop_gain_posicao;
         
         ObjectSetDouble(0, hl_stop_gain_atual, OBJPROP_PRICE, stop_gain_atual_pontos);
         ObjectSetDouble(0, hl_stop_loss_atual, OBJPROP_PRICE, stop_loss_atual_pontos);

         Print("(comprado) - price: ", DoubleToString(price_close[1], 0), " - stop_loss: ", DoubleToString(stop_loss_atual_pontos, 0), " - stop_gain: ", DoubleToString(stop_gain_atual_pontos, 0), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
         return true;
      }
      
      Print("Error: trade.buy - ResultRetcode: ", trade.ResultRetcode());
      return false;
   }
   
   Print("Error: trade.buy - antes de verificar o ResultRetcode");
   return false; 
}

bool se_posicionar_vendido(double tamanho_posicao, double stop_loss_posicao, double stop_gain_posicao) {
   
   double stop_loss_ordem = stop_loss_posicao + tamanho_posicao*30;
   double stop_gain_ordem = stop_gain_posicao - tamanho_posicao*30;
   
   bool res_trade_sell = trade.Sell(lote, _Symbol, 0, normalize_pontos(stop_loss_ordem), normalize_pontos(stop_gain_ordem), "zerado->vendido");
   
   if(res_trade_sell){
   
      if(trade.ResultRetcode() == 10008 || trade.ResultRetcode() == 10009) {
    
         stop_loss_atual_pontos = stop_loss_posicao;
         stop_gain_atual_pontos = stop_gain_posicao;
         
         ObjectSetDouble(0, hl_stop_gain_atual, OBJPROP_PRICE, stop_gain_atual_pontos);
         ObjectSetDouble(0, hl_stop_loss_atual, OBJPROP_PRICE, stop_loss_atual_pontos);
         
         Print("(vendido) - price: ", DoubleToString(price_close[1], 0), " - stop_loss: ", DoubleToString(stop_loss_atual_pontos, 0), " - stop_gain: ", DoubleToString(stop_gain_atual_pontos, 0), " - qtd_bars: ", IntegerToString(qtd_bars_dia));
         return true;
      }
      
      Print("Error: trade.sell - ResultRetcode: ", trade.ResultRetcode());
      return false;
   }
   
   Print("Error: trade.sell - antes de verificar o ResultRetcode");
   return false; 
}

